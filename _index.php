<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Dermaglass disfarça instantaneamente a aparência das linhas de expressão, olheiras deixando você com a aparência mais jovem. Proporcionando o famoso efeito cinderela.">
      <meta name="author" content="Dermaglass">
      <meta name="Robots" content="index, follow">
      <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
      <link rel="icon" href="/img/favicon.ico" type="image/x-icon">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">
      <!-- Facebook stuff -->
      <meta property="og:title" content="Dermaglass, disfarça instantaneamente a aparência das linhas de expressão, olheiras deixando você com a aparência mais jovem.Dermaglass é um sérum tensor que disfarça instantaneamente a aparência das linhas de expressão, olheiras deixando você com a aparência mais jovem." />
      <meta property="og:image" content="http://www.dermaglass.com.br/img/antes-depois.png" />
      <meta property="og:url" content="http://www.dermaglass.com.br" />
      <meta property="og:description" content="Dermaglass é um produto inovador, proporcionando o famoso "efeito cinderela", em poucos minutos você percebe a diminuição das rugas e melhoras na aparência da pele" />
      <meta property="og:title" content="Dermaglass Eternally Young" />
      <title>Dermaglass -  deixa você com a aparência mais jovem</title>

      <!-- <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
      <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link href="css/noto.css" rel="stylesheet">
      <link href="css/dermaglass.css" rel="stylesheet">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type='text/javascript' src='js/loadImg.js'></script>
      <script type='text/javascript' src='js/StackBlur.js'></script>
      <script type='text/javascript' src='js/rgbcolor.js'></script>
      <script type='text/javascript' src='js/canvg.js'></script>
      <script type='text/javascript' src='js/dermaglass.js'></script>
      <script src="js/grayscale.js"></script>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
         .btn{
           display: none!important;
         }
       </style>
   </head>

   <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
      <div class="se-pre-con">
         <canvas id="canvas" class="center-div"></canvas>
      </div>
      <!-- Navigation -->
      <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header ">
                    <a class="navbar-brand page-scroll" href="#page-top">
                    <b>Dermaglass</b>
                    </a>
                    <a href="#" type="button" id="btn-comprar" class="btn btn-custom btn-comprar navbar-btn pull-right">
                    <span class="glyphicon"><b>COMPRAR AGORA</b></span>
                    </a>
          </div>
        </div>
      </nav>
      <!-- Amostra Section -->
      <div id="amostra" class="container content-section text-center">
         <div class="row">
            <div class="col-lg-12 apresentacao">
               <div id="container">
                  <div id="slideimg0" class="slide showMe">
                     <img  src="img/Antes-M.png">
                     <div class='background-label-imagem-apresentacao'><span class='label-imagem-apresentacao'>ANTES</span></div>
                  </div>
                  <div id="slideimg1" class="slide ">
                     <img src="img/Depois-M.png">
                     <div class='background-label-imagem-apresentacao'><span class='label-imagem-apresentacao'>DEPOIS</span></div>
                  </div>
                  <div id="slideimg2" class="slide ">
                     <img src="img/Antes-H.png">
                     <div class='background-label-imagem-apresentacao'><span class='label-imagem-apresentacao'>ANTES</span></div>
                  </div>
                  <div id="slideimg3" class="slide ">
                     <img  src="img/Depois-H.png">
                     <div class='background-label-imagem-apresentacao'><span class='label-imagem-apresentacao'>DEPOIS</span></div>
                  </div>
                  <div id="slideimg4" class="slide ">
                     <img  src="img/dermaglass_1140-434px.jpg">
                     <div class='background-label-imagem-apresentacao'>
                        <a type="button" class="btn btn-custom btn-comprar navbar-btn"><b>COMPRAR AGORA</b></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div  class="content-section-a">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 apresentacao">
                  <h2>Apresentação</h2>
                  <p>Dermaglass é um sérum tensor que disfarça instantaneamente a aparência das linhas de expressão, olheiras deixando você com a aparência mais jovem.</p>
                  <p>Dermaglass é um produto inovador, proporcionando o famoso "efeito cinderela", em poucos minutos você percebe a diminuição das rugas e melhoras na aparência da pele.</p>
               </div>
               <div class="col-lg-6">
                  <img src="http://dermaglass.com.br/img/antes-depois.jpg" width="400" height="300" />
               </div>
            </div>
         </div>
      </div>
      <div id="exemplos" class="content-section-b">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h2> PRINCIPAIS BENEFÍCIOS </h2>
                  <ul>
                     <li> Diminui a aparência de rugas;</li>
                     <li>Apaga a intensidade das olheiras;</li>
                     <li>Reduz o inchaço (bolsa) sob os olhos;</li>
                     <li>Minimiza a aparência dos poros;</li>
                     <li>Ajuda a equilibrar a textura da pele;</li>
                     <li>Restaura a firmeza da pele reduzindo a flacidez;</li>
                     <li>Rápida Absorção pela pele, deixando a pele livre de oleosidade;</li>
                     <li>Tempo de duração de até 8 horas.</li>
                  </ul>
               </div>
               <div class="col-lg-6">
                  <h2> MODO DE USAR </h2>
                  <br>
                  <div class="row">
                     <div class="col-lg-4 modo-de-usar">
                        <img src="img/dermaglass-modo-de-usar-01.png" /><br>
                        <p>Aplique uma fina camada com a ponta dos dedos nas áreas desejadas.</p>
                     </div>
                     <div class="col-lg-4 modo-de-usar">
                        <img src="img/dermaglass-modo-de-usar-02.png"  /><br>
                        <p>Aguarde alguns minutos até secar, permanecendo inexpressivo(a).</p>
                     </div>
                     <div class="col-lg-4 modo-de-usar">
                        <img src="img/dermaglass-modo-de-usar-03.png"  /><br>
                        <p>Pronto! Se surgirem resíduos brancos, remova com algodão úmido.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="satisfacao" class="content-section-a">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h2> SATISFAÇÃO </h2>
                  <p><b>Testes de Eficácia Relatório RE-TA325-15-01B-R0</b></p>
                  <p>Opinião dos voluntários</p>
                  <ul>
                     <li>O produto proporcionou o efeito tensor - 96,7%</li>
                     <li>O produto </li>
                     <li>O produto reduziou/suavisou as linhas de expressão na região dos lábios - 90%</li>
                     <li>O produto melhorou a firmeza da pele - 96,7%</li>
                     <li>O produto reduziu/suavizou as linhas de expressão na região da testa - 83,3%</li>
                     <li>O produto reduziu/suavizou as linhas de expressão na região do pescoço - 83,3%</li>
                     <li>O produto proporcionou o efeito porcelana na pele - 83,3%</li>
                     <li>O produto diminuiu a intensidade das olheiras - 73,3%</li>
                  </ul>
               </div>
               <div class="col-lg-6" style="position: relative;    padding-top: 15%;">
                  <div class="skinn-box">
                     <p> 99,9% das voluntárias se sentiram até 15 anos mais jovem após 3 minutos da aplicação do produto</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="produto" class="content-section-b">
         <div class="container">
            <div class="row" style="text-align: center;">
               <img src="img/produto.png" width="700px"/>
               <div class="span12">
                  <div class="center text-center">
                     <div class="center text-center">
                        <h2 style="text-align: center;">Clique em comprar agora e pague no boleto ou em até 3 vezes</h2>
                        <div>
                           <br>
                           <a type="button" class="btn btn-custom btn-comprar navbar-btn">
                           <span class="glyphicon"></span><b>COMPRAR AGORA</b>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>

      <footer>
          <div class="container">
              <div class="row-fluid">
                  <div class="span12">
                  </div>
                  <hr>
                  <div class="row-fluid">
                      <div class="span12">
                          <div class="span4">
                            <p class="muted pull-right" style="font-size: 12pt;">Fabricado por: Dermavita Ind. e Com. Prod. Farm. Ltda<br>
                                CNPJ: 85.314.029/0001-09     <br>
                                Aut. Func. 2.03219-4             <br>
                                Av. Dom Joaquim, 405 – Jd. Maluche<br>
                                Brusque - SC<br>
                                Responsável Técnico: Wanderlei Willrich   CRF/ SC 2312<br>
                                E-mail: <a href="mailto:vendas@dermaglass.com.br?Subject=Contato" target="_top">vendas@dermaglass.com.br</a><br>
                            </p>
                          </div>
                      </div>
                  </div>
              </div>
              <p>Todos os direitos reservados © Dermaglass 2016</p>
              <br>
              <p style="font-size: 10pt; display:none;">Desenvolvido por <a href="mailto:natanloterio@gmail.com">Natan Lotério</a></p>
          </div>

      </footer>



            <!-- start Mixpanel -->
            <script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
               0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
               for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
               mixpanel.init("a7c9ddd3b85ab65bc4b0226ce5edf6fa");mixpanel.track("view");
            </script><!-- end Mixpanel -->
            <script>
               (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
               })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

               ga('create', 'UA-87252144-1', 'auto');
               ga('send', 'pageview');
               // google
            </script>

   </body>
</html>
