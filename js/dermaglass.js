var timer;
var curImage = 0;
var numImages = 5;

function comprar(){
    $("#form-comprar").submit();
}

function subscribe(){
    var email_subscribe = $("#email").val();

    mixpanel.track("contato",{"$email":email_subscribe});
    $("#email").val("");

    showToast();

    $.post("email.php",{email: email_subscribe},
        function(data, status){
            console.log(status);
        });

}

function showToast() {
    $("#snackbar").show(300);
    setTimeout(function(){ $("#snackbar").hide(300); }, 3000);
}


$(document).ready(function(){
    canvg(document.getElementById('canvas'),'<!--?xml version="1.0" encoding="utf-8"?--><svg width="134px" height="134px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-dashinfinity"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><path d="M24.3,30C11.4,30,5,43.3,5,50s6.4,20,19.3,20c19.3,0,32.1-40,51.4-40C88.6,30,95,43.3,95,50s-6.4,20-19.3,20C56.4,70,43.6,30,24.3,30z" fill="none" stroke="#e7a287" stroke-width="8s" stroke-dasharray="118.03090698242188 138.55802124023438" stroke-dashoffset="0"><animate attributeName="stroke-dashoffset" from="0" to="256.58892822265625" begin="0" dur="1s" repeatCount="indefinite" fill="freeze"></animate></path></svg>');
    $(".btn-comprar").on("click",function(){
        //comprar();
      //  $("#shopping-cart").show(400);
        //indow.location = "checkout-transparente/"
    });

    $(".btn-success").on('click',function(){
        $("#shopping-cart").hide(400);
    });
    $("#close-shopping-cart").on('click',function(){
        $("#shopping-cart").hide(400);
    });

    $("#confirmar-pedido").on('click',function(){
        $("#pedido").hide();
        $("#escolher-forma-pagamento").show();
    });

    $("#btn-cartao-credito").on('click',function(){
        $("#escolher-forma-pagamento").hide();
        $("#info-cartao").show();
    });

    startCarrousel();

});

$(window).on('load',function() {
    $(".se-pre-con").fadeOut("slow");
    $("#amostra").height($(".slide").height());
});

$(window).resize(function() {
    $("#amostra").height($("#slideimg0").height());
});

$(function(){

    var imagens_desktop = ["img/Antes-M.png","img/Depois-M.png","img/Antes-H.png","img/Depois-H.png"]
    var imagens_mobile = ["img/Antes-M_mobile.png","img/Depois-M_mobile.png","img/Antes-H_mobile.png","img/Depois-H_mobile.png"]
    var sources = imagens_desktop;

    if(isMobile()){
        sources = imagens_mobile;
    }

    $(".slide").each(function(i){

        $(this).attr("src",sources[i]);

    });

    //$('img').imgPreload();

});

function isMobile(){
    return window.screen.width < 768;
}

function startCarrousel() {
  timer = setInterval(nextImage, 4000);
}

function nextImage() {
    var e;
    // remove showMe class from current image
    e = document.getElementById("slideimg" + curImage);
    removeClass(e, "showMe");

    // compute next image
    curImage++;
    if (curImage > numImages - 1) {
        curImage = 0;
    }

    // add showMe class to next image
    e = document.getElementById("slideimg" + curImage);
    addClass(e, "showMe");
}

function addClass(elem, name) {
    var c = elem.className;
    if (c) c += " ";  // if not blank, add a space separator
    c += name;
    elem.className = c;
}

function removeClass(elem, name) {
    var c = elem.className;
    elem.className = c.replace(name, "").replace(/   /g, " ").replace(/^ | $/g, "");  // remove name and extra blanks
}



 function getcardbrand(callback){
  var numero = document.getElementById("creditCardNumber").value;
  PagSeguroDirectPayment.getBrand({
    cardBin: numero,
    success: callback,
    error: function(response) {
      log(response);
    },
    complete: function(response) {
      //log(response);
    }
  });
 }

 function log(str){
   if(str instanceof Object){
     str = JSON.stringify(str);
   }
   console.log(str);
 }

 function enviarPagamento(){

  hash = PagSeguroDirectPayment.getSenderHash();
  log("hash:"+hash);

  var valor = document.getElementById("quantidade").value * 180;
  log("Valor:"+valor);

  PagSeguroDirectPayment.getPaymentMethods({
    amount: valor,
    success: function(response){
      //log(response);
      salvarBandeiras(response.paymentMethods);
      getcardbrand(function(response) {
        salvarDadosCartao(response.brand);
        //carregarImageBandeira();
        var numero = document.getElementById("creditCardNumber").value;
        var cvv = document.getElementById("creditCardCVC").value;
        var expiry = document.getElementById("creditCardExpiry").value;
        expiry = expiry.split("/");
        var mesexp = expiry[0];
        var anoexp = "20"+expiry[1];
        var brand = cartao.name;
        createCardToken(numero,brand,cvv,mesexp,anoexp,function(response){
          token = response.card.token;
          log(response)
        });
    }	);
    },
    error: log
  });

 }

 function carregarImageBandeira(){
   var src = getBrandImage(cartao.name.toUpperCase());
   document.getElementById("card-brand").src = src;
 }

 function salvarDadosCartao(b){
   cartao = b;
 }

 function salvarBandeiras(paymentMethods){
   cartoes = paymentMethods.CREDIT_CARD.options;
 }

 function getBrandImage(brand){
  var src = eval("cartoes."+brand+".images.SMALL.path");
  return "https://stc.pagseguro.uol.com.br"+src;
 }

 function createCardToken(numero,bandeira,codigo_serguranca,mes_exp, ano_exp,callback){
  PagSeguroDirectPayment.createCardToken({
    cardNumber: numero,
    brand: bandeira,
    cvv: codigo_serguranca,
    expirationMonth: mes_exp,
    expirationYear: ano_exp,
    success: callback,
    error: log,
    complete: log
  });
 }

 function prepareCheckout(){
   var clientInfo = {};
   clientInfo.boxQuantity = $("#quantidade").val();//
   clientInfo.clientName = $("#creditCardHolderName").val();//
   clientInfo.clientCPF = $("#creditCardHolderCPF").val();//
   clientInfo.clientPhone = $("#clientPhone").val().replace("-","");//
   clientInfo.clientEmail = $("#clientEmail").val();//
   clientInfo.clientAreaCode =  $("#clientAreaCode").val();//
   clientInfo.shippingAddressStreet = $("#shippingAddressStreet").val();//
   clientInfo.shippingAddressNumber = $("#shippingAddressNumber").val();//
   clientInfo.shippingAddressComplement = $("#shippingAddressComplement").val();//
   clientInfo.shippingAddressDistrict = $("#shippingAddressDistrict").val();//
   clientInfo.shippingAddressCity = $("#shippingAddressCity").val();//
   clientInfo.shippingAddressPostalCode = $("#shippingAddressPostalCode").val();//
   clientInfo.shippingAddressState = $("#shippingAddressState").val();//
   if($('input[name=tipo_frete]:checked').val()){
     clientInfo.shippingType = $('input[name=tipo_frete]:checked').val();
   }
   return clientInfo;
 }

 function creditCardCheckout(token,numParcelas,valorParcela, creditCardHolderName,creditCardHolderCPF){
   var paymentData = prepareCheckout();
   for (var attrname in clientInfo) { paymentData[attrname] = clientInfo[attrname]; };
   paymentData.clientHash = hash;
   paymentData.creditCardToken = token;
   paymentData.paymentMethod = "creditcard";
   paymentData.installmentQuantity = numParcelas;
   paymentData.installmentValue = valorParcela;
   paymentData.noInterestInstallmentQuantity = 0;
   paymentData.creditCardHolderName = creditCardHolderName;
   paymentData.creditCardHolderCPF =  creditCardHolderCPF;

   $.post("pagseguro/checkout.php", paymentData, function(result){
      log(result);
    });

 }

 function formatarDec(valor){
   return parseFloat(Math.round(valor * 100) / 100).toFixed(2);
 }
 function comprar(){
     $("#form-comprar").submit();
 }

 function subscribe(){
     var email_subscribe = $("#email").val();

     mixpanel.track("contato",{"$email":email_subscribe});
     $("#email").val("");

     showToast();

     $.post("email.php",{email: email_subscribe},
         function(data, status){
             console.log(status);
         });

 }

 function showToast() {
     $("#snackbar").show(300);
     setTimeout(function(){ $("#snackbar").hide(300); }, 3000);
 }
