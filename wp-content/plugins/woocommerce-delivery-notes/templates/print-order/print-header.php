<?php
/**
 * Print order header
 *
 * @package WooCommerce Print Invoice & Delivery Note/Templates
 */

if ( !defined( 'ABSPATH' ) ) exit;

function get_cpf_from_ecfb( $fields, $order ) {
  $new_fields = array();

  if( get_post_meta( $order->id, '_billing_cpf', true ) ) {
    $new_fields['_billing_cpf'] = array(
      'label' => 'CPF',
      'value' => get_post_meta( $order->id, '_billing_cpf', true )
    );
  }

  if( get_post_meta( $order->id, '_billing_rg', true ) ) {
    $new_fields['_billing_rg'] = array(
      'label' => 'RG',
      'value' => get_post_meta( $order->id, '_billing_rg', true )
    );
  }


  return array_merge( $fields, $new_fields );
}
function abreviar_estados($arrayEstados){
  $estado = trim($arrayEstados['state']);
  $estadosBrasileiros = array(
  'Acre'=>'AC',
  'Alagoas'=>'AL',
  'Amapá'=>'AP',
  'Amazonas'=>'AM',
  'Bahia'=>'BA',
  'Ceará'=>'CE',
  'Distrito Federal'=>'DF',
  'Espírito Santo'=>'ES',
  'Goiás'=>'GO',
  'Maranhão'=>'MA',
  'Mato Grosso'=>'MT',
  'Mato Grosso do Sul'=>'MS',
  'Minas Gerais'=>'MG',
  'Pará'=>'PA',
  'Paraíba'=>'PB',
  'Paraná'=>'PR',
  'Pernambuco'=>'PE',
  'Piauí'=>'PI',
  'Rio de Janeiro'=>'RJ',
  'Rio Grande do Norte'=>'RN',
  'Rio Grande do Sul'=>'RS',
  'Rondônia'=>'RO',
  'Roraima'=>'RR',
  'Santa Catarina'=>'SC',
  'São Paulo'=>'SP',
  'Sergipe'=>'SE',
  'Tocantins'=>'TO'
  );

  $arrayEstados['city'] = $arrayEstados['city'].' - '.$estado;
  $arrayEstados['state'] = '';
  return $arrayEstados;
}


add_filter('woocommerce_order_formatted_shipping_address','abreviar_estados');

add_filter( 'wcdn_order_info_fields', 'get_cpf_from_ecfb', 10, 2 );



?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?php wcdn_document_title(); ?></title>

	<?php
		// wcdn_head hook
		do_action( 'wcdn_head' );
	?>
</head>

<body class="<?php echo wcdn_get_template_type(); ?>">

	<div id="container">

		<?php
			// wcdn_head hook
			do_action( 'wcdn_before_page' );
		?>

		<div id="page">
