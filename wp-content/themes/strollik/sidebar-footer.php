<?php
/**
 * The Footer Sidebar
 *
 * @package WPOPAL
 * @subpackage strollik
 * @since Strollik 1.0
 */

?>
 
		
	<section class="footer-top">
		<div class="container">
			<div class="row">
				<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
						<div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="600ms" >
							<?php dynamic_sidebar('footer-1'); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="800ms" >
							<?php dynamic_sidebar('footer-2'); ?>
						</div>
					</div> 
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="footer-bottom">
		<div class="container">
			<div class="row">
				<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
					<div class="<?php { if ( is_active_sidebar( 'footer-4' ) ) { echo 'col-lg-8 col-md-8 ';} else { echo 'col-lg-12 col-md-12'; }} ?>col-sm-12 col-xs-12">
						<div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="200ms" >
							<?php dynamic_sidebar('footer-3'); ?>
						</div>
					</div>
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
					<div class="<?php { if ( is_active_sidebar( 'footer-3' ) ) { echo 'col-lg-4 col-md-4 ';} else { echo 'col-lg-12 col-md-12'; }} ?>col-sm-12 col-xs-12">
						<div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="400ms" >
							<?php dynamic_sidebar('footer-4'); ?>
						</div>
					</div>
				<?php endif; ?>

				
			</div>
		</div>
	</section>
 