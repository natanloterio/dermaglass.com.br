<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$_id = strollik_fnc_makeid();

?>


<div class="widget pbr-designs<?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php if($title!=''){ ?>
        <h3 class="widget-title visual-title">
            <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
    <div class="widget-content">
    	<div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play" data-ride="owlcarousel">
            <div class="carousel-controls carousel-controls-v3 carousel-hidden">
                <a class="left carousel-control carousel-md" href="#" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control carousel-md" href="#" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                </a>
            </div>
            <div class="owl-carousel" data-slide="1" data-pagination="false" data-navigation="true">
				<?php for ($i = 1; $i <= 6 ; $i++) { ?>
					<?php if ( !empty($atts['design_title'.$i]) || !empty($atts['design_description'.$i]) ) { ?>
						<div class="item media">
							<?php if ( !empty($atts['design_image'.$i]) ) { ?>
								<div class="image pull-left hidden-sm hidden-xs">
									<?php echo wp_get_attachment_image( $atts['design_image'.$i], 'full' ); ?>
								</div>
							<?php } ?>
							<div class="content body-media text-center">
								<?php if ( !empty($atts['design_title'.$i])) { ?>
									<h3 class="design_title"><?php echo trim( $atts['design_title'.$i] ); ?></h3>
								<?php } ?>
								<?php if ( !empty($atts['design_description'.$i])) { ?>
									<div class="design_description"><?php echo trim( $atts['design_description'.$i] ); ?></div>
								<?php } ?>
								<?php if ( !empty($atts['design_video'.$i])) { ?>
									<div class="button-video"><button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#videoModal<?php echo esc_attr( $i ); ?>">
										<i class="fa fa-play-circle-o"></i><?php esc_html_e('See It In Action', 'strollik'); ?>
									</button></div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php for ($i = 1; $i <= 6 ; $i++) { ?>
	<?php if ( !empty($atts['design_video'.$i])) { ?>
		<div class="modal fade videoModal" id="videoModal<?php echo esc_attr( $i ); ?>" tabindex="-1" role="dialog">
		  	<div class="modal-dialog modal-lg" role="document">
	        	<?php echo wp_oembed_get( $atts['design_video'.$i] ); ?>
		  	</div>
		  	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
		    </div>
		</div>
	<?php } ?>
<?php } ?>