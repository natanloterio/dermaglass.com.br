<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_register_script('spritespin-rotate-js', get_template_directory_uri().'/js/spritespin.js', array( 'jquery' ), '20150315', true);
wp_enqueue_script('spritespin-rotate-js');

$_id = strollik_fnc_makeid();
$images_url = array();
$images = !empty($images) ? explode(',', $images) : array();
foreach ($images as $value) {
	$img_url = wp_get_attachment_url( $value );
	if ( !empty($img_url) ) {
		$images_url[] = esc_url($img_url);
	}
}
?>
<section class="widget pbr-rotateimages<?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php if($title!=''){ ?>
        <h3 class="widget-title visual-title">
            <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
    <div class="widget-content">
	    <div class="rotate">
	    	<div id="rotateimages-<?php echo esc_attr($_id); ?>">
			    
			</div>
		</div>
    </div>
</section>
<?php if ( count($images_url) > 0 ): ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$("#rotateimages-<?php echo esc_attr($_id); ?>").spritespin({
			  source: [<?php echo '"'.implode('","', $images_url).'"'; ?>],
			  width   : 877,
			  height  : 732,
			});
		})
		
	</script>
<?php endif; ?>