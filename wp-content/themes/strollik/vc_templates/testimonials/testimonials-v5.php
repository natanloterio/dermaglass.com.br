<?php $job = get_post_meta( get_the_ID(), 'strollik_testimonial_job', true); ?>
<div class="testimonials-body">
    <p class="testimonials-description"><?php the_content() ?></p>                            
    <h5 class="testimonials-name">
         <?php the_title(); ?>
    </h5>  
    <?php if(!empty($job) ): ?>
    	<div class="job"><?php echo trim($job); ?></div>
	<?php endif; ?>  
</div>