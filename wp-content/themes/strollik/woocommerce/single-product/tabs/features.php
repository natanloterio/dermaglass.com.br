<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'strollik_woocommerce_product_features_heading', esc_html__( 'Product Features', 'strollik' ) ) );
$features = get_post_meta( get_the_ID(), 'strollik_product_features', true);
?>

<?php if ( $heading ): ?>
  <h2><?php echo trim( $heading ); ?></h2>
<?php endif; ?>

<?php echo trim( $features ); ?>
