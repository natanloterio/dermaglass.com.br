<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

$per_page = strollik_fnc_theme_options('woo-number-product-single', 6);

$upsells = $product->get_upsells();

if ( sizeof( $upsells ) == 0 ) {
	return;
}

$meta_query = WC()->query->get_meta_query();

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $posts_per_page,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array( $product->id ),
	'meta_query'          => $meta_query
);

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = strollik_fnc_theme_options('product-number-columns', 3);

if ( $products->have_posts() ) : ?>

	<div class="upsells products">
	<div class="widget-text-heading"><h3 class="widget-heading heading-style1"><span class="heading-text"><?php esc_html_e( 'You may also like&hellip;', 'strollik' ) ?></span></h3></div>

		<?php woocommerce_product_loop_start(); ?>

			<?php wc_get_template( 'widget-products/carousel.php',array( 'loop'=>$products,'columns_count'=> $woocommerce_loop['columns'], 'posts_per_page'=> $products->post_count ) ); ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>

<?php endif;

wp_reset_postdata();
