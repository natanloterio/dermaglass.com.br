<?php
/**
 * Shop breadcrumb
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if(is_single()) return; 
if ( ! empty( $breadcrumb )) {

	$delimiter = '<span>&#62;</span>';

	echo trim($wrap_before);

	$end = '' ;
	$title = '';
	foreach ( $breadcrumb as $key => $crumb ) {

		echo trim($before);
		echo '<li>';

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} else {
			echo esc_html( $crumb[0] );
			$title = esc_html( $crumb[0] );
		}

		echo trim($after);
		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo trim( $delimiter );
		}
		echo '</li>';

		$end = esc_html( $crumb[0] );
	}
	printf('<li  class="active"><h2>%s</h2></li>', $title);
	echo trim($wrap_after);

}
?>
