<?php

function strollik_woocommerce_enqueue_scripts() {
	wp_enqueue_script( 'strollik-woocommerce', get_template_directory_uri() . '/js/woocommerce.js', array( 'jquery', 'suggest' ), '20131022', true );
}
add_action( 'wp_enqueue_scripts', 'strollik_woocommerce_enqueue_scripts' );


/**
 */
add_filter('add_to_cart_fragments', 'strollik_fnc_woocommerce_header_add_to_cart_fragment' );

function strollik_fnc_woocommerce_header_add_to_cart_fragment( $fragments ){
	global $woocommerce;

	$fragments['#cart .mini-cart-items'] =  sprintf(_n(' <span class="mini-cart-items"> %d  </span> ', ' <span class="mini-cart-items"> %d <em>item</em> </span> ', $woocommerce->cart->cart_contents_count, 'strollik'), $woocommerce->cart->cart_contents_count);
 	$fragments['#cart .mini-cart-total'] = trim( $woocommerce->cart->get_cart_total() );
    
    return $fragments;
}

/**
 * Mini Basket
 */
if(!function_exists('strollik_fnc_minibasket')){
    function strollik_fnc_minibasket( $style='' ){ 
        $template =  apply_filters( 'wpopal_fnc_minibasket_template', strollik_fnc_get_header_layout( '' )  );  
        
      //  if( $template == 'v4' ){
        //	$template = 'v3';
     //   }
       	
        return get_template_part( 'woocommerce/cart/mini-cart-button', $template ); 
    }
}
if(strollik_fnc_theme_options("woo-show-minicart",true)){
	add_action( 'strollik_template_header_right', 'strollik_fnc_minibasket', 30, 0 );
}
/******************************************************
 * 												   	  *
 * Hook functions applying in archive, category page  *
 *												      *
 ******************************************************/
function strollik_template_woocommerce_main_container_class( $class ){ 
	if( is_product() ){
		$class .= ' '.  strollik_fnc_theme_options('woocommerce-single-layout') ;
	}else if( is_product_category() || is_archive()  ){ 
		$class .= ' '.  strollik_fnc_theme_options('woocommerce-archive-layout') ;
	}
	return $class;
}
add_filter( 'strollik_template_woocommerce_main_container_class', 'strollik_template_woocommerce_main_container_class' );


function strollik_fnc_get_woocommerce_sidebar_configs( $configs='' ){

	global $post; 
	$right = null; $left = null; 

	if( is_product() ){
		$left  	=  strollik_fnc_theme_options( 'woocommerce-single-left-sidebar', 'sidebar-left' ); 
		$right 	=  strollik_fnc_theme_options( 'woocommerce-single-right-sidebar', 'sidebar-right' );
		$layout =  strollik_fnc_theme_options( 'woocommerce-single-layout', 'mainright');  

	}else if( is_product_category() || is_archive() ){
		$left  	=  strollik_fnc_theme_options( 'woocommerce-archive-left-sidebar', 'sidebar-left' ); 
		$right 	=  strollik_fnc_theme_options( 'woocommerce-archive-right-sidebar', 'sidebar-right' );
		$layout =  strollik_fnc_theme_options( 'woocommerce-archive-layout', 'mainright' ); 
	}

 
	return strollik_fnc_get_layout_configs( $layout, $left, $right, $configs );
}

add_filter( 'strollik_fnc_get_woocommerce_sidebar_configs', 'strollik_fnc_get_woocommerce_sidebar_configs', 1, 1 );


function strollik_woocommerce_breadcrumb_defaults( $args ){
	$args['wrap_before'] = '<div class="pbr-breadscrumb"><div class="container"><ol class="pbr-woocommerce-breadcrumb breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';
	$args['wrap_after'] = '</ol></div></div>';

	return $args;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'strollik_woocommerce_breadcrumb_defaults' );

add_action( 'strollik_woo_template_main_before', 'woocommerce_breadcrumb', 30, 0 );
/**
 * Remove show page results which display on top left of listing products block.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 10 );


function strollik_fnc_woocommerce_after_shop_loop_start(){
	echo '<div class="products-bottom-wrap clearfix">';
}

add_action( 'woocommerce_after_shop_loop', 'strollik_fnc_woocommerce_after_shop_loop_start', 1 );

function strollik_fnc_woocommerce_after_shop_loop_after(){
	echo '</div>';
}

add_action( 'woocommerce_after_shop_loop', 'strollik_fnc_woocommerce_after_shop_loop_after', 10000 );


/**
 * Wrapping all elements are wrapped inside Div Container which rendered in woocommerce_before_shop_loop hook
 */
function strollik_woocommerce_before_shop_loop_start(){
	echo '<div class="products-top-wrap clearfix">';
}

function strollik_woocommerce_before_shop_loop_end(){
	echo '</div>';
}


add_action( 'woocommerce_before_shop_loop', 'strollik_woocommerce_before_shop_loop_start' , 0 );
add_action( 'woocommerce_before_shop_loop', 'strollik_woocommerce_before_shop_loop_end' , 1000 );


function strollik_fnc_woocommerce_display_modes(){
	global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	$woo_display = 'grid';
	if ( isset($_COOKIE['strollik_woo_display']) && $_COOKIE['strollik_woo_display'] == 'list' ) {
		$woo_display = $_COOKIE['strollik_woo_display'];
	}
	echo '<form action="'.  $current_url  .'" class="display-mode" method="get">';
 		
 		if( isset($_GET['product_cat']) ){
			echo '<input type="hidden" name="product_cat" value="'.$_GET['product_cat'].'">';
		}
 		
		echo '<button title="'.esc_html__('Grid','strollik').'" class="btn '.($woo_display == 'grid' ? 'active' : '').'" value="grid" name="display" type="submit"><i class="fa fa-th"></i></button>';	
		echo '<button title="'.esc_html__( 'List', 'strollik' ).'" class="btn '.($woo_display == 'list' ? 'active' : '').'" value="list" name="display" type="submit"><i class="fa fa-th-list"></i></button>';	
	echo '</form>'; 
}

add_action( 'woocommerce_before_shop_loop', 'strollik_fnc_woocommerce_display_modes' , 10 );


add_filter( 'yith_wcwl_button_label',          'strollik_fnc_woocomerce_icon_wishlist'  );
    add_filter( 'yith-wcwl-browse-wishlist-label', 'strollik_fnc_woocomerce_icon_wishlist_add' );


    function strollik_fnc_woocomerce_icon_wishlist( $value='' ){
    	return '<i class="fa fa-heart-o"></i><span>' .esc_html__('Wishlist','strollik').'</span>';
    }

    function strollik_fnc_woocomerce_icon_wishlist_add(){
    	return '<i class="fa fa-heart-o"></i><span>' .esc_html__('Wishlist','strollik').'</span>';
    }


/**
 * Processing hook layout content
 */
function strollik_fnc_layout_main_class( $class ){
	$sidebar = strollik_fnc_theme_options( 'woo-sidebar-show', 1 ) ;
	if( is_single() ){
		$sidebar = strollik_fnc_theme_options('woo-single-sidebar-show'); ;
	}
	else {
		$sidebar = strollik_fnc_theme_options('woo-sidebar-show'); 
	}

	if( $sidebar ){
		return $class;
	}

	return 'col-lg-12 col-md-12 col-xs-12';
}
add_filter( 'strollik_woo_layout_main_class', 'strollik_fnc_layout_main_class', 4 );


/**
 *
 */
function strollik_fnc_woocommerce_archive_image(){ 
	if ( is_tax( array( 'product_cat', 'product_tag' ) ) && get_query_var( 'paged' ) == 0 ) { 
		$thumb =  get_woocommerce_term_meta( get_queried_object()->term_id, 'thumbnail_id', true ) ;

		if( $thumb ){
			$img = wp_get_attachment_image_src( $thumb, 'full' ); 
		
			echo '<p class="category-banner"><img src="'.$img[0].'""></p>'; 
		}
	}
}
add_action( 'woocommerce_archive_description', 'strollik_fnc_woocommerce_archive_image', 9 );
/**
 * Add action to init parameter before processing
 */

function strollik_fnc_before_woocommerce_init(){
	if( isset($_GET['display']) && ($_GET['display']=='list' || $_GET['display']=='grid') ){  
		setcookie( 'strollik_woo_display', trim($_GET['display']) , time()+3600*24*100,'/' );
		$_COOKIE['strollik_woo_display'] = trim($_GET['display']);
	}
}

add_action( 'init', 'strollik_fnc_before_woocommerce_init' );
/***************************************************
 * 												   *
 * Hook functions applying in single product page  *
 *												   *
 ***************************************************/


/** 
 * Remove review to products tabs. and display this as block below the tab.
 */
function strollik_fnc_woocommerce_product_tabs( $tabs ){

	if( isset($tabs['reviews']) ){
		unset( $tabs['reviews'] ); 
	}
	return $tabs;
}
//add_filter( 'woocommerce_product_tabs','strollik_fnc_woocommerce_product_tabs', 99 );
 
 /**
  * Rehook product review products in woocommerce_after_single_product_summary
  */
function strollik_fnc_product_reviews(){
	return comments_template();
}
//add_action('woocommerce_after_single_product_summary','strollik_fnc_product_reviews', 10 );
 


function strollik_woocommerce_show_product_thumbnails( $layout ){ 
	$layout = $layout.'-h';
	return $layout;
}

//add_filter( 'pbrthemer_woocommerce_show_product_thumbnails', 'strollik_woocommerce_show_product_thumbnails'  );


function strollik_woocommerce_show_product_images( $layout ){ 
	$layout = $layout.'-h';
	return $layout;
}

//add_filter( 'pbrthemer_woocommerce_show_product_images', 'strollik_woocommerce_show_product_images'  );

/**
 * Show/Hide related, upsells products
 */
function strollik_woocommerce_related_upsells_products($located, $template_name) {
	$options      = get_option('pbr_theme_options');
	$content_none = get_template_directory() . '/woocommerce/content-none.php';

	if ( 'single-product/related.php' == $template_name ) {
		if ( isset( $options['wc_show_related'] ) && 
			( 1 == $options['wc_show_related'] ) ) {
			$located = $content_none;
		}
	} elseif ( 'single-product/up-sells.php' == $template_name ) {
		if ( isset( $options['wc_show_upsells'] ) && 
			( 1 == $options['wc_show_upsells'] ) ) {
			$located = $content_none;
		}
	}

	return apply_filters( 'strollik_woocommerce_related_upsells_products', $located, $template_name );
}

add_filter( 'wc_get_template', 'strollik_woocommerce_related_upsells_products', 10, 2 );

/**
 * Number of products per page
 */
function strollik_woocommerce_shop_per_page($number) {
	$value = strollik_fnc_theme_options('woo-number-page', get_option('posts_per_page'));
	if ( is_numeric( $value ) && $value ) {
		$number = absint( $value );
	}
	return $number;
}

add_filter( 'loop_shop_per_page', 'strollik_woocommerce_shop_per_page' );

/**
 * Number of products per row
 */
function strollik_woocommerce_shop_columns($number) {
	$value = strollik_fnc_theme_options('wc_itemsrow', 3);
	if ( in_array( $value, array(2, 3, 4, 6) ) ) {
		$number = $value;
	}
	return $number;
}

add_filter( 'loop_shop_columns', 'strollik_woocommerce_shop_columns' );

function strollik_fnc_woocommerce_share_box() {
	if ( strollik_fnc_theme_options('wc_show_share_social', 1) ) {
		get_template_part( 'page-templates/parts/sharebox' );
	}
}
add_filter( 'woocommerce_single_product_summary', 'strollik_fnc_woocommerce_share_box', 100 );

//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


function strollik_fnc_product_features() {
	wc_get_template( 'single-product/tabs/features.php' );
}

function strollik_fnc_woocommerce_products_tab($tabs){
	global $post;
	if( isset($tabs['reviews']) ){
		unset( $tabs['reviews'] ); 
	}
	if( isset($tabs['additional_information']) ){
		unset( $tabs['additional_information'] ); 
	}
	$features = get_post_meta( $post->ID, 'strollik_product_features', true);
	if ( !empty($features) ) {
		$tabs['features'] = array(
			'title'    => esc_html__( 'Features', 'strollik' ),
			'priority' => 11,
			'callback' => 'strollik_fnc_product_features'
		);
	}
	$tabs['reviews'] = array(
		'title'    => esc_html__( 'Reviews', 'strollik' ),
		'priority' => 20,
		'callback' => 'strollik_fnc_product_reviews'
	);

	return $tabs;
}
add_filter('woocommerce_product_tabs', 'strollik_fnc_woocommerce_products_tab', 100);

function strollik_fnc_woocommerce_product_thumbnails_columns() {
	return strollik_fnc_theme_options('woo-number-thumbnail-single', 4);

}
add_filter('woocommerce_product_thumbnails_columns', 'strollik_fnc_woocommerce_product_thumbnails_columns');







