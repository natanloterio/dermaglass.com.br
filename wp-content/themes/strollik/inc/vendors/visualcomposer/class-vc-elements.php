<?php 

class Strollik_VC_Elements implements Vc_Vendor_Interface {

	public function load(){ 
		
		/*********************************************************************************************************************
		 *  Our Service
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Featured Box",'strollik'),
		    "base" => "pbr_featuredbox",
		
		    "description"=> esc_html__('Decreale Service Info', 'strollik'),
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',    "admin_label" => true,
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'strollik' ),
				    'param_name' => 'title_color',
				    'description' => esc_html__( 'Select font color', 'strollik' )
				),

		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Sub Title", 'strollik'),
					"param_name" => "subtitle",
					"value" => '',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'strollik'),
					"param_name" => 'style',
					'value' 	=> array(
						esc_html__('Default', 'strollik') => 'default', 
						esc_html__('Version 1', 'strollik') => 'v1', 
						esc_html__('Version 2', 'strollik') => 'v2', 
						esc_html__('Version 3', 'strollik' )=> 'v3',
						esc_html__('Version 4', 'strollik') => 'v4'
					),
					'std' => ''
				),

				array(
					'type'                           => 'dropdown',
					'heading'                        => esc_html__( 'Title Alignment', 'strollik' ),
					'param_name'                     => 'title_align',
					'value'                          => array(
					esc_html__( 'Align left', 'strollik' )   => 'separator_align_left',
					esc_html__( 'Align center', 'strollik' ) => 'separator_align_center',
					esc_html__( 'Align right', 'strollik' )  => 'separator_align_right'
					),
					'std' => 'separator_align_left'
				),

			 	array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'strollik'),
					"param_name" => "icon",
					"value" => 'fa fa-gear',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'strollik' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'strollik' ) . '</a>'
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Icon Color', 'strollik' ),
				    'param_name' => 'color',
				    'description' => esc_html__( 'Select font color', 'strollik' )
				),	
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Background Icon', 'strollik' ),
					'param_name' => 'background',
					'value' => array(
						esc_html__( 'None', 'strollik' ) => 'nostyle',
						esc_html__( 'Success', 'strollik' ) => 'bg-success',
						esc_html__( 'Info', 'strollik' ) => 'bg-info',
						esc_html__( 'Danger', 'strollik' ) => 'bg-danger',
						esc_html__( 'Warning', 'strollik' ) => 'bg-warning',
						esc_html__( 'Light', 'strollik' ) => 'bg-default',
					),
					'std' => 'nostyle',
				),

				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'strollik'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'strollik'),
					"param_name" => "information",
					"value" => 'Your Description Here',
					'description'	=> esc_html__('Allow  put html tags', 'strollik' )
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
		 
	   	/*********************************************************************************************************************
		 * Pricing Table
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Pricing",'strollik'),
		    "base" => "pbr_pricing",
		    "description" => esc_html__('Make Plan for membership', 'strollik' ),
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Price", 'strollik'),
					"param_name" => "price",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Currency", 'strollik'),
					"param_name" => "currency",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Period", 'strollik'),
					"param_name" => "period",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Subtitle", 'strollik'),
					"param_name" => "subtitle",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Is Featured", 'strollik'),
					"param_name" => "featured",
					'value' 	=> array(  esc_html__('No', 'strollik') => 0,  esc_html__('Yes', 'strollik') => 1 ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Skin", 'strollik'),
					"param_name" => "skin",
					'value' 	=> array(  esc_html__('Skin 1', 'strollik') => 'v1',  esc_html__('Skin 2', 'strollik') => 'v2', esc_html__('Skin 3', 'strollik') => 'v3' ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Box Style", 'strollik'),
					"param_name" => "style",
					'value' 	=> array( 'boxed' => esc_html__('Boxed', 'strollik')),
				),

				array(
					"type" => "textarea_html",
					"heading" => esc_html__("Content", 'strollik'),
					"param_name" => "content",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'strollik')
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Link Title", 'strollik'),
					"param_name" => "linktitle",
					"value" => 'SignUp',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Link", 'strollik'),
					"param_name" => "link",
					"value" => 'http://yourdomain.com',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
 		

 		/*********************************************************************************************************************
		 *  PBR Counter
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Counter",'strollik'),
		    "base" => "pbr_counter",
		    "class" => "",
		    "description"=> esc_html__('Counting number with your term', 'strollik'),
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',
					"admin_label"	=> true
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Description", 'strollik'),
					"param_name" => "description",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number", 'strollik'),
					"param_name" => "number",
					"value" => ''
				),

			 	array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'strollik'),
					"param_name" => "icon",
					"value" => '',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'strollik' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'strollik' ) . '</a>'
				),


				array(
					"type" => "attach_image",
					"description" => esc_html__("If you upload an image, icon will not show.", 'strollik'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> esc_html__('Image', 'strollik' )
				),

		 

				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Text Color", 'strollik'),
					"param_name" => "text_color",
					'value' 	=> '',
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
		
		/*
		 *  Design
		 */
		$fields = array();
		$fields[] = array(
			"type" => "textfield",
			"heading" => esc_html__("Title", 'strollik'),
			"param_name" => "title",
			"value" => '',
			"admin_label"	=> true
		);
		for ($i = 1; $i <= 6; $i++) {
			$fields[] = array(
				"type" => "textfield",
				"heading" => esc_html__("Design Title", 'strollik').' '.$i,
				"param_name" => "design_title".$i,
				"value" => '',
				"admin_label"	=> true
			);
			$fields[] = array(
				"type" => "textarea",
				"heading" => esc_html__("Design Description", 'strollik').' '.$i,
				"param_name" => "design_description".$i,
				"value" => ''
			);
			$fields[] = array(
				"type" => "textfield",
				"heading" => esc_html__("Design Video Link", 'strollik').' '.$i,
				"param_name" => "design_video".$i,
				"value" => ''
			);
			$fields[] = array(
				"type" => "attach_image",
				"description" => esc_html__("Design Image", 'strollik'),
				"param_name" => "design_image".$i,
				"value" => '',
				'heading'	=> esc_html__('Image', 'strollik' )
			);
		}
		$fields[] = array(
			"type" => "textfield",
			"heading" => esc_html__("Extra class name", 'strollik'),
			"param_name" => "el_class",
			"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
		);

		vc_map( array(
		    "name" => esc_html__("PBR Designs",'strollik'),
		    "base" => "pbr_designs",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => $fields
		));



		$fields = array();
		$fields[] = array(
			"type" => "textfield",
			"heading" => esc_html__("Title", 'strollik'),
			"param_name" => "title",
			"value" => '',
			"admin_label"	=> true
		);
		$fields[] = array(
			"type" => "attach_images",
			"description" => esc_html__("Image", 'strollik'),
			"param_name" => "images",
			"value" => '',
			'heading'	=> esc_html__('Image', 'strollik' )
		);
		$fields[] = array(
			"type" => "textfield",
			"heading" => esc_html__("Extra class name", 'strollik'),
			"param_name" => "el_class",
			"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
		);
		vc_map( array(
		    "name" => esc_html__("PBR Rotate Images",'strollik'),
		    "base" => "pbr_rotateimages",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',
					"admin_label"	=> true
				),
				array(
					"type" => "attach_images",
					"description" => esc_html__("Image", 'strollik'),
					"param_name" => "images",
					"value" => '',
					'heading'	=> esc_html__('Image', 'strollik' )
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
	    	)
		));
	}
}