<?php 
class Strollik_VC_News implements Vc_Vendor_Interface  {
	
	public function load(){
		 
		$newssupported = true; 
 
			/**********************************************************************************
			 * Front Page Posts
			 **********************************************************************************/


			/// Front Page 1
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 1', 'strollik' ),
				'base' => 'pbr_frontpageposts',
				'icon' => 'icon-wpb-news-1',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

					 
					 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			// front page 2
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 2', 'strollik' ),
				'base' => 'pbr_frontpageposts2',
				'icon' => 'icon-wpb-news-8',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

			 
					 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			// front page 3
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 3', 'strollik' ),
				'base' => 'pbr_frontpageposts3',
				'icon' => 'icon-wpb-news-3',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

					 

					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			// front page 2
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 4', 'strollik' ),
				'base' => 'pbr_frontpageposts4',
				'icon' => 'icon-wpb-news-4',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

				 
				 

					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					 
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			
				// front page 12
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 12', 'strollik' ),
				'base' => 'pbr_frontpageposts12',
				'icon' => 'icon-wpb-news-12',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

				 
					 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					 
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
		// frontpage 13
			vc_map( array(
				'name' => esc_html__( '(News) FontPage 13', 'strollik' ),
				'base' => 'pbr_frontpageposts13',
				'icon' => 'icon-wpb-news-13',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Categories Tab Hovering to show post', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

					

					 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					 
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );	

				/**********************************************************************************
			 * FontPage News 14
			 **********************************************************************************/
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 14', 'strollik' ),
				'base' => 'pbr_frontpageposts14',
				'icon' => 'icon-wpb-news-1',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

				 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			
		vc_map( array(
				'name' => esc_html__( '(News) Categories Post', 'strollik' ),
				'base' => 'pbr_categoriespost',
				'icon' => 'icon-wpb-news-3',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

					 

					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					 

					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );	

		// front page 9
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 9', 'strollik' ),
				'base' => 'pbr_frontpageposts9',
				'icon' => 'icon-wpb-news-9',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),
 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Grid Columns", 'strollik'),
						"param_name" => "grid_columns",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );

			// front page 3
			vc_map( array(
				'name' => esc_html__( '(Blog) TimeLine Post', 'strollik' ),
				'base' => 'pbr_timelinepost',
				'icon' => 'icon-wpb-news-10',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),
 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					 

					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Enable Pagination', 'strollik' ),
						'param_name' => 'pagination',
						'value' => array( 'No'=>'0', 'Yes'=>'1'),
						'std' => '0',
						'admin_label' => true,
						'description' => esc_html__( 'Select style display.', 'strollik' )
					)
				)
			) );

			/****/
			vc_map( array(
				'name' => esc_html__( '(News) Categories Tab Post', 'strollik' ),
				'base' => 'pbr_categorytabpost',
				'icon' => 'icon-wpb-application-icon-large',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

				 
					 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),

				 

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),

					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
			

			$layout_image = array(
				esc_html__('Grid', 'strollik')             => 'grid-1',
				esc_html__('List', 'strollik')             => 'list-1',
				esc_html__('List not image', 'strollik')   => 'list-2',
			);
			
			vc_map( array(
				'name' => esc_html__( '(News) Grid Posts', 'strollik' ),
				'base' => 'pbr_gridposts',
				'icon' => 'icon-wpb-news-2',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Post having news,managzine style', 'strollik' ),
			 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),
 
				 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Layout Type", 'strollik'),
						"param_name" => "layout",
						"layout_images" => $layout_image,
						"value" => $layout_image,
						"admin_label" => true,
						"description" => esc_html__("Select Skin layout.", 'strollik')
					),

					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Grid Columns", 'strollik'),
						"param_name" => "grid_columns",
						"value" => array( 1 , 2 , 3 , 4 , 6),
						"std" => 3
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );


			
			/**********************************************************************************
			 * Mega Blogs
			 **********************************************************************************/

			/// Front Page 1
			vc_map( array(
				'name' => esc_html__( '(Blog) FrontPage', 'strollik' ),
				'base' => 'pbr_frontpageblog',
				'icon' => 'icon-wpb-news-1',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),
		 			 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );


			vc_map( array(
				'name' => esc_html__('(Blog) Grids ', 'strollik' ),
				'base' => 'pbr_megablogs',
				'icon' => 'icon-wpb-news-2',
				"category" => esc_html__('PBR News', 'strollik'),
				'description' => esc_html__( 'Create Post having blog styles', 'strollik' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'strollik' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
						"admin_label" => true
					),

				 

				 
					array(
						'type' => 'textarea',
						'heading' => esc_html__( 'Description', 'strollik' ),
						'param_name' => 'descript',
						"value" => ''
					),

					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'strollik' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 10 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
					),
					
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Layout", 'strollik' ),
						"param_name" => "layout",
						"value" => array( esc_html__('Default Style', 'strollik' ) => 'blog' , esc_html__('Default Style 2', 'strollik' ) => 'blog-v2'  ,  esc_html__('Special Style 1', 'strollik' ) => 'special-1',  esc_html__('Special Style 2', 'strollik' ) => 'special-2',  esc_html__('Special Style 3', 'strollik' ) => 'special-3' ),
						"std" => 3,
						'admin_label'=> true
					),

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Grid Columns", 'strollik'),
						"param_name" => "grid_columns",
						"value" => array( 1 , 2 , 3 , 4 , 6),
						"std" => 3
					),
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
						'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'strollik' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
					)
				)
			) );
		 

			/**********************************************************************************
			 * Slideshow Post Widget Gets
			 **********************************************************************************/
				vc_map( array(
					'name' => esc_html__( '(News) Slideshow Post', 'strollik' ),
					'base' => 'pbr_slideshopbrst',
					'icon' => 'icon-wpb-news-slideshow',
					"category" => esc_html__('PBR News', 'strollik'),
					'description' => esc_html__( 'Play Posts In slideshow', 'strollik' ),
					 
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Widget title', 'strollik' ),
							'param_name' => 'title',
							'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'strollik' ),
							"admin_label" => true
						),

					 

						array(
							'type' => 'textarea',
							'heading' => esc_html__( 'Heading Description', 'strollik' ),
							'param_name' => 'descript',
							"value" => ''
						),

						array(
							'type' => 'loop',
							'heading' => esc_html__( 'Grids content', 'strollik' ),
							'param_name' => 'loop',
							'settings' => array(
								'size' => array( 'hidden' => false, 'value' => 10 ),
								'order_by' => array( 'value' => 'date' ),
							),
							'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'strollik' )
						),

						array(
							"type" => "dropdown",
							"heading" => esc_html__("Layout", 'strollik' ),
							"param_name" => "layout",
							"value" => array( esc_html__('Default Style', 'strollik' ) => 'blog'  ,  esc_html__('Special Style 1', 'strollik' ) => 'style1' ,  esc_html__('Special Style 2', 'strollik' ) => 'style2' ),
							"std" => 3
						),

						array(
							"type" => "dropdown",
							"heading" => esc_html__("Grid Columns", 'strollik'),
							"param_name" => "grid_columns",
							"value" => array( 1 , 2 , 3 , 4 , 6),
							"std" => 3
						),
						array(
							'type' => 'checkbox',
							'heading' => esc_html__( 'Show Pagination Links', 'strollik' ),
							'param_name' => 'show_pagination',
							'description' => esc_html__( 'Enables to show paginations to next new page.', 'strollik' ),
							'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
						),

						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Thumbnail size', 'strollik' ),
							'param_name' => 'thumbsize',
							'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'strollik' )
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Extra class name', 'strollik' ),
							'param_name' => 'el_class',
							'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
						)
					)
				) );
 
	}
}