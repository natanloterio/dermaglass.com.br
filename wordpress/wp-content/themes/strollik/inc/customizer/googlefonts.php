<?php 

function strollik_google_fonts_customizer( $wp_customize ){

	$wp_customize -> add_section( 'typography_options', array(
		'title' => esc_html__( 'Typography Options', 'strollik' ),
		'description' => esc_html__('Modify Fonts','strollik'),
		'priority' => 6
	));
}
add_action( 'customize_register', 'strollik_google_fonts_customizer' );