<?php 
/**
 * Remove javascript and css files not use
 */


/**
 * Hoo to top bar layout
 */
function strollik_fnc_topbar_layout(){
	$layout = strollik_fnc_get_header_layout();
	if(strollik_fnc_theme_options('topbar', true)){
		get_template_part( 'page-templates/parts/topbar', $layout );
	}
	get_template_part( 'page-templates/parts/topbar', 'mobile' );
}

add_action( 'strollik_template_header_before', 'strollik_fnc_topbar_layout' );

/**
 * Hook to select header layout for archive layout
 */
function strollik_fnc_get_header_layout( $layout='' ){
	global $post; 

	$layout = $post && get_post_meta( $post->ID, 'strollik_header_layout', 1 ) ? get_post_meta( $post->ID, 'strollik_header_layout', 1 ) : strollik_fnc_theme_options( 'headerlayout' );
	 
 	if( $layout ){
 		return trim( $layout );
 	}elseif ( $layout = strollik_fnc_theme_options('header_skin','') ){
 		return trim( $layout );
 	}

	return $layout;
} 

add_filter( 'strollik_fnc_get_header_layout', 'strollik_fnc_get_header_layout' );

/**
 * Hook to select header layout for archive layout
 */
function strollik_fnc_get_footer_profile( $profile='default' ){

	global $post; 

	$profile =  $post? get_post_meta( $post->ID, 'strollik_footer_profile', 1 ):null ;

 	if( $profile ){
 		return trim( $profile );
 	}elseif ( $profile = strollik_fnc_theme_options('footer-style', $profile ) ){  
 		return trim( $profile );
 	}

	return $profile;
} 

add_filter( 'strollik_fnc_get_footer_profile', 'strollik_fnc_get_footer_profile' );


/**
 * Render Custom Css Renderig by Visual composer
 */
if ( !function_exists( 'strollik_fnc_print_style_footer' ) ) {
	function strollik_fnc_print_style_footer(){
		$footer =  strollik_fnc_get_footer_profile( 'default' );
		if($footer!='default'){
			$shortcodes_custom_css = get_post_meta( $footer, '_wpb_shortcodes_custom_css', true );
			if ( ! empty( $shortcodes_custom_css ) ) {
				echo '<style>
					'.$shortcodes_custom_css.'
					</style>';
			}
		}
	}
	add_action('wp_head','strollik_fnc_print_style_footer', 18);
}


/**
 * Hook to show breadscrumbs
 */
function wpopal_fnc_render_breadcrumbs(){
	
	global $post;

	if( is_object($post) ){
		$disable = get_post_meta( $post->ID, 'strollik_disable_breadscrumb', 1 );  
		if(  $disable || is_front_page() ){
			return true; 
		}
		$bgimage = get_post_meta( $post->ID, 'strollik_image_breadscrumb', 1 );  
		$color 	 = get_post_meta( $post->ID, 'strollik_color_breadscrumb', 1 );  
		$bgcolor = get_post_meta( $post->ID, 'strollik_bgcolor_breadscrumb', 1 );  
		$style = array();
		if( $bgcolor  ){
			$style[] = 'background-color:'.$bgcolor;
		}
		if( $bgimage  ){ 
			$style[] = 'background-image:url(\''.wp_get_attachment_url($bgimage).'\')';
		}

		if( $color  ){ 
			$style[] = 'color:'.$color;
		}

		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	}else {
		$estyle = ''; 
	}
	
	echo '<section id="pbr-breadscrumb" class="pbr-breadscrumb" '.$estyle.'><div class="container">';
			strollik_fnc_breadcrumbs();
	echo '</div></section>';

}

add_action( 'strollik_template_main_before', 'wpopal_fnc_render_breadcrumbs' ); 

 
/**
 * Main Container
 */

function strollik_template_main_container_class( $class ){
	global $post; 
	global $strollik_wpopconfig;

	$layoutcls = get_post_meta( $post->ID, 'strollik_enable_fullwidth_layout', 1 );
	
	if( $layoutcls ) {
		$strollik_wpopconfig['layout'] = 'fullwidth';
		return 'container-fluid';
	}
	return $class;
}
add_filter( 'strollik_template_main_container_class', 'strollik_template_main_container_class', 1 , 1  );
add_filter( 'strollik_template_main_content_class', 'strollik_template_main_container_class', 1 , 1  );



function strollik_template_footer_before(){
	return get_sidebar( 'newsletter' );
}

add_action( 'strollik_template_footer_before', 'strollik_template_footer_before' );


/**
 * Get Configuration for Page Layout
 *
 */
function strollik_fnc_get_page_sidebar_configs( $configs='' ){

	global $post; 

	$left  	=  get_post_meta( $post->ID, 'strollik_leftsidebar', 1 );
	$right 	=  get_post_meta( $post->ID, 'strollik_rightsidebar', 1 );
	$layout =  get_post_meta( $post->ID, 'strollik_page_layout', 1 );

	return strollik_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'strollik_fnc_get_page_sidebar_configs', 'strollik_fnc_get_page_sidebar_configs', 1, 1 );


function strollik_fnc_get_single_sidebar_configs( $configs='' ){

	global $post; 

	$layout =  strollik_fnc_theme_options( 'blog-single-layout', 'fullwidth');
	$left  	=  strollik_fnc_theme_options( 'blog-single-left-sidebar', 'blog-sidebar-left');
	$right 	=  strollik_fnc_theme_options( 'blog-single-right-sidebar', 'blog-sidebar-right');

	return strollik_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'strollik_fnc_get_single_sidebar_configs', 'strollik_fnc_get_single_sidebar_configs', 1, 1 );

function strollik_fnc_get_archive_sidebar_configs( $configs='' ){

	$left  	=  strollik_fnc_theme_options( 'blog-archive-left-sidebar', 'blog-sidebar-left'); 
	$right 	=  strollik_fnc_theme_options( 'blog-archive-right-sidebar', 'blog-sidebar-right');
	$layout =  strollik_fnc_theme_options( 'blog-archive-layout', 'fullwidth'); 
 	
	return strollik_fnc_get_layout_configs($layout, $left, $right, $configs);
}

add_filter( 'strollik_fnc_get_archive_sidebar_configs', 'strollik_fnc_get_archive_sidebar_configs', 1, 1 );
add_filter( 'strollik_fnc_get_category_sidebar_configs', 'strollik_fnc_get_archive_sidebar_configs', 1, 1 );

function strollik_fnc_sidebars_others_configs(){
	
	global $strollik_page_layouts;
	
	return $strollik_page_layouts; 
}

add_filter("strollik_fnc_sidebars_others_configs", "strollik_fnc_sidebars_others_configs");


function strollik_fnc_get_layout_configs($layout, $left, $right, $configs= array()){
	
    switch ($layout) {
    
	    // Two Sidebar
	    case 'leftmainright':
	    	$configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-lg-3 col-md-3 col-xs-12'
				),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-3'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-6');
	    break;

	    //One Sidebar Right
	    case 'mainright':
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-3'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-9');
	    break;

	    // One Sidebar Left
	    case 'leftmain':
	        $configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-xs-12 col-md-3'
				),
				'right' => array('show'  	=> false)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-9');
	    break;

	    // Fullwidth
	    default:
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array('show'  	=> false )
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-12');
	        break;
	    }

    return $configs;
}

if(!function_exists('strollik_fnc_related_post')){
    function strollik_fnc_related_post( $relate_count = 4, $posttype = 'post', $taxonomy = 'category' ){
      
        $terms = get_the_terms( get_the_ID(), $taxonomy );
        $termids =array();

        if($terms){
            foreach($terms as $term){
                $termids[] = $term->term_id;
            }
        }

        $args = array(
            'post_type' => $posttype,
            'posts_per_page' => $relate_count,
            'post__not_in' => array( get_the_ID() ),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'id',
                    'terms' => $termids,
                    'operator' => 'IN'
                )
            )
        );
        $template_name = 'related_'.$posttype.'.php';

        $relates = new WP_Query( $args );

        if (is_file(STROLLIK_PBR_THEME_DIR.'/page-templates/' . $template_name)) {
            include(STROLLIK_PBR_THEME_DIR.'/page-templates/' . $template_name);
        }
    }
}