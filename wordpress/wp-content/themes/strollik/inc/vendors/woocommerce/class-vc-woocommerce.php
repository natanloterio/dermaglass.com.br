<?php 
if( class_exists("WPBakeryShortCode") ){
	/**
	 * Class Strollik_VC_Woocommerces
	 *
	 */
	class Strollik_VC_Woocommerce  implements Vc_Vendor_Interface  {

		/**
		 * register and mapping shortcodes
		 */
		//add_shortcode( 'pbr_products', ('adela_pbr_vc_shortcode_render')  );
		public function product_category_field_search( $search_string ) {
			$data = array();
			$vc_taxonomies_types = array('product_cat');
			$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
				'hide_empty' => false,
				'search' => $search_string
			) );
			if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
				foreach ( $vc_taxonomies as $t ) {
					if ( is_object( $t ) ) {
						$data[] = vc_get_term_object( $t );
					}
				}
			}
			return $data;
		}
		

		public function product_category_render($query) {  
			$category = get_term_by('id', (int)$query['value'], 'product_cat');
			if ( ! empty( $query ) && !empty($category)) {
				$data = array();
				$data['value'] = $category->term_id;
				$data['label'] = $category->name;
				return ! empty( $data ) ? $data : false;
			}
			return false;
		}

		/**
		 * register and mapping shortcodes
		 */
		public function load(){  

			$shortcodes = array( 'pbr_categoriestabs', 'pbr_products', 'pbr_products_collection' ); 

			foreach( $shortcodes as $shortcode ){
				add_filter( 'vc_autocomplete_'. $shortcode .'_categories_callback', array($this, 'product_category_field_search'), 10, 1 );
			 	add_filter( 'vc_autocomplete_'. $shortcode .'_categories_render', array($this, 'product_category_render'), 10, 1 );
			}


			$order_by_values = array(
				'',
				esc_html__( 'Date', 'strollik' ) => 'date',
				esc_html__( 'ID', 'strollik' ) => 'ID',
				esc_html__( 'Author', 'strollik' ) => 'author',
				esc_html__( 'Title', 'strollik' ) => 'title',
				esc_html__( 'Modified', 'strollik' ) => 'modified',
				esc_html__( 'Random', 'strollik' ) => 'rand',
				esc_html__( 'Comment count', 'strollik' ) => 'comment_count',
				esc_html__( 'Menu order', 'strollik' ) => 'menu_order',
			);

			$order_way_values = array(
				'',
				esc_html__( 'Descending', 'strollik' ) => 'DESC',
				esc_html__( 'Ascending', 'strollik' ) => 'ASC',
			);
			$product_categories_dropdown = array(''=> esc_html__('None', 'strollik') );
			$block_styles = strollik_fnc_get_widget_block_styles();
			
			$product_columns_deal = array(1, 2, 3, 4);

			if( is_admin() ){
					$args = array(
						'type' => 'post',
						'child_of' => 0,
						'parent' => '',
						'orderby' => 'name',
						'order' => 'ASC',
						'hide_empty' => false,
						'hierarchical' => 1,
						'exclude' => '',
						'include' => '',
						'number' => '',
						'taxonomy' => 'product_cat',
						'pad_counts' => false,

					);

					$categories = get_categories( $args );
					strollik_fnc_woocommerce_getcategorychilds( 0, 0, $categories, 0, $product_categories_dropdown );
					
			}
		    vc_map( array(
		        "name" => esc_html__("PBR Product Deals",'strollik'),
		        "base" => "pbr_product_deals",
		        "class" => "",
		    	"category" => esc_html__('PBR Woocommerce','strollik'),
		    	'description'	=> esc_html__( 'Display Product Sales with Count Down', 'strollik' ),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Title','strollik'),
		                "param_name" => "title",
		            ),

		             array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),

		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Extra class name", 'strollik'),
		                "param_name" => "el_class",
		                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Number items to show", 'strollik'),
		                "param_name" => "number",
		                'std' => '1',
		                "description" => esc_html__("", 'strollik')
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__("Columns count",'strollik'),
		                "param_name" => "columns_count",
		                "value" => $product_columns_deal,
		                'std' => '2',
		                "description" => esc_html__("Select columns count.",'strollik')
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__("Layout",'strollik'),
		                "param_name" => "layout",
		                "value" => array(esc_html__('Carousel', 'strollik') => 'carousel', esc_html__('Grid', 'strollik') =>'grid' ),
		                "admin_label" => true,
		                "description" => esc_html__("Select columns count.",'strollik')
		            )
		        )
		    ));
		   

			vc_map( array(
		        "name" => esc_html__("PBR Timing Deals",'strollik'),
		        "base" => "pbr_timing_deals",
		        "class" => "",
		    	"category" => esc_html__('PBR Woocommerce','strollik'),
		    	'description'	=> esc_html__( 'Display Product Sales with Count Down', 'strollik' ),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Title','strollik'),
		                "param_name" => "title",
		                "admin_label" => true
		            ),
		             array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
		             array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Desciption','strollik'),
		                "param_name" => "description",
		                "admin_label" => false
		            ),
		             array(
					    'type' => 'textfield',
					    'heading' => esc_html__( 'End Date', 'strollik' ),
					    'param_name' => 'input_datetime',
					    'description' => esc_html__( 'Enter Date Count down', 'strollik' ),
					    "value" => ""

					),
		             array(
		                "type" => "dropdown",
		                "heading" => esc_html__("Columns count",'strollik'),
		                "param_name" => "columns_count",
		                "value" => array(6,5,4,3,2,1),
		                "admin_label" => false,
		                "description" => esc_html__("Select columns count.",'strollik')
		            ),

		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Extra class name", 'strollik'),
		                "param_name" => "el_class",
		                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
		            )

		            
		        )
		    ));
	 
		    //// 
		    vc_map( array(
		        "name" => esc_html__( "PBR Products On Sale", 'strollik' ),
		        "base" => "pbr_products_onsale",
		        "class" => "",
		    	"category" => esc_html__( 'PBR Woocommerce', 'strollik' ),
		    	'description'	=> esc_html__( 'Display Products Sales With Pagination', 'strollik' ),
		        "params" => array(
		            array(
		                "type" 		  => "textfield",
		                "class" 	  => "",
		                "heading" 	  => esc_html__( 'Title','strollik' ),
		                "param_name"  => "title",
		                "admin_label" => true
		            ),
		             array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => esc_html__("Number items to show", 'strollik'),
		                "param_name" => "number",
		                'std' => '9',
		                "description" => esc_html__("", 'strollik'),
		                  "admin_label" => true
		            ),
		             array(
		                "type" 		  => "dropdown",
		                "heading" 	  => esc_html__( "Columns count",'strollik' ),
		                "param_name"  => "columns_count",
		                "value" 	  => array(6,5,4,3,2,1),
		                "admin_label" => false,
		                "description" => esc_html__( "Select columns count.",'strollik' )
		            ),

		            array(
		                "type" 		  => "textfield",
		                "heading" 	  => esc_html__( "Extra class name", 'strollik' ),
		                "param_name"  => "el_class",
		                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
		            )
		        )
		    ));
		  
			/**
			 * pbr_productcategory
			 */
		 

			$product_layout = array('Grid'=>'grid','List'=>'list','Carousel'=>'carousel', 'Special'=>'special', 'List-v1' => 'list-v1');
			$product_type = array('Best Selling'=>'best_selling','Featured Products'=>'featured_product','Top Rate'=>'top_rate','Recent Products'=>'recent_product','On Sale'=>'on_sale','Recent Review' => 'recent_review' );
			$product_columns = array(6 ,5 ,4 , 3, 2, 1);
			$show_tab = array(
			                array('recent', esc_html__('Latest Products', 'strollik')),
			                array( 'featured_product', esc_html__('Featured Products', 'strollik' )),
			                array('best_selling', esc_html__('BestSeller Products', 'strollik' )),
			                array('top_rate', esc_html__('TopRated Products', 'strollik' )),
			                array('on_sale', esc_html__('Special Products', 'strollik' ))
			            );

			vc_map( array(
			    "name" => esc_html__("PBR Product Category",'strollik'),
			    "base" => "pbr_productcategory",
			    "class" => "",
			 "category" => esc_html__('PBR Woocommerce','strollik'),
			     'description'=> esc_html__( 'Show Products In Carousel, Grid, List, Special','strollik' ), 
			    "params" => array(
			    	array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__('Title', 'strollik'),
						"param_name" => "title",
						"value" =>''
					),
					 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
			    	array(
						"type" => "dropdown",
						"class" => "",
						"heading" => esc_html__('Category', 'strollik'),
						"param_name" => "category",
						"value" => $product_categories_dropdown,
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style",'strollik'),
						"param_name" => "style",
						"value" => $product_layout
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number of products to show",'strollik'),
						"param_name" => "number",
						"value" => '4'
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns count",'strollik'),
						"param_name" => "columns_count",
						"value" => array(6, 5, 4, 3, 2, 1),
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name",'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));
			 
			vc_map( array(
			    "name" => esc_html__("PBR Products Category Index",'strollik'),
			    "base" => "pbr_productcategory_index",
			    "class" => "",
				 "category" => esc_html__('PBR Woocommerce','strollik'),
			     'description'=> esc_html__( 'Show Products In Carousel, Grid, List, Special','strollik' ), 
			    "params" => array(
			    	array(
						"type" => "textfield",
						"class" => "",
						"heading" => esc_html__('Title', 'strollik'),
						"param_name" => "title",
						"value" =>''
					),

					 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
			    	array(
						"type" => "dropdown",
						"class" => "",
						"heading" => esc_html__('Category', 'strollik'),
						"param_name" => "category",
						"value" => $product_categories_dropdown,
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style",'strollik'),
						"param_name" => "style",
						"value" => $product_layout
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number of products to show",'strollik'),
						"param_name" => "number",
						"value" => '4'
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns count",'strollik'),
						"param_name" => "columns_count",
						"value" => array(6, 5, 4, 3, 2, 1),
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Block Styles",'strollik'),
						"param_name" => "block_style",
						"value" => $block_styles,
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Icon",'strollik'),
						"param_name" => "icon",
						'value'	=> 'fa-gear'
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name",'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));
			 
			/**
			* pbr_category_filter
			*/
		 
			vc_map( array(
					"name"     => esc_html__("PBR Product Categories Filter",'strollik'),
					"base"     => "pbr_category_filter",
					'description' => esc_html__( 'Show images and links of sub categories in block','strollik' ),
					"class"    => "",
					"category" => esc_html__('PBR Woocommerce','strollik'),
					"params"   => array(

					array(
						"type" => "dropdown",
						"heading" => esc_html__('Category', 'strollik'),
						"param_name" => "term_id",
						"value" =>$product_categories_dropdown,	"admin_label" => true
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'strollik'),
						"param_name" => "style",
						'value' 	=> array(
							esc_html__('Default', 'strollik') => '', 
							esc_html__('style 1', 'strollik') => 'style1',
						),
						'std' => ''
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories (190px x 190px)", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),

					array(
						"type"       => "textfield",
						"heading"    => esc_html__("Number of categories to show",'strollik'),
						"param_name" => "number",
						"value"      => '5',

					),

					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Extra class name",'strollik'),
						"param_name"  => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));

			/**
			 * pbr_products
			 */
			vc_map( array(
			    "name" => esc_html__("PBR Products",'strollik'),
			    "base" => "pbr_products",
			    'description'=> esc_html__( 'Show products as bestseller, featured in block', 'strollik' ),
			    "class" => "",
			   "category" => esc_html__('PBR Woocommerce','strollik'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title",'strollik'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => ''
					),
					 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
					array(
					    'type' => 'autocomplete',
					    'heading' => esc_html__( 'Categories', 'strollik' ),
					    'value' => '',
					    'param_name' => 'categories',
					    "admin_label" => true,
					    'description' => esc_html__( 'Select Categories', 'strollik' ),
					    'settings' => array(
					     'multiple' => true,
					     'unique_values' => true,
					     // In UI show results except selected. NB! You should manually check values in backend
					    ),
				   	),
			    	array(
						"type" => "dropdown",
						"heading" => esc_html__("Type",'strollik'),
						"param_name" => "type",
						"value" => $product_type,
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style",'strollik'),
						"param_name" => "style",
						"value" => $product_layout
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns count",'strollik'),
						"param_name" => "columns_count",
						"value" => $product_columns,
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number of products to show",'strollik'),
						"param_name" => "number",
						"value" => '4'
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name",'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));
			 

			/**
			 * pbr_all_products
			 */
			vc_map( array(
			    "name" => esc_html__("PBR Products Tabs",'strollik'),
			    "base" => "pbr_tabs_products",
			    'description'	=> esc_html__( 'Display BestSeller, TopRated ... Products In tabs', 'strollik' ),
			    "class" => "",
			   "category" => esc_html__('PBR Woocommerce','strollik'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title",'strollik'),
						"param_name" => "title",
						"value" => ''
					),
					 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
					array(
			            "type" => "sorted_list",
			            "heading" => esc_html__("Show Tab", 'strollik'),
			            "param_name" => "show_tab",
			            "description" => esc_html__("Control teasers look. Enable blocks and place them in desired order.", 'strollik'),
			            "value" => "recent,featured_product,best_selling",
			            "options" => $show_tab
			        ),
			        array(
						"type" => "dropdown",
						"heading" => esc_html__("Style",'strollik'),
						"param_name" => "style",
						"value" => $product_layout
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number of products to show",'strollik'),
						"param_name" => "number",
						"value" => '4'
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns count",'strollik'),
						"param_name" => "columns_count",
						"value" => $product_columns,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name",'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));

			vc_map( array(
				"name"     => esc_html__("PBR Product Categories List",'strollik'),
				"base"     => "pbr_category_list",
				"class"    => "",
				"category" => esc_html__('PBR Woocommerce','strollik'),
				'description' => esc_html__( 'Show Categories as menu Links', 'strollik' ),
				"params"   => array(
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_html__('Title', 'strollik'),
					"param_name" => "title",
					"value"      => '',
				),
				 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show post counts', 'strollik' ),
					'param_name' => 'show_count',
					'description' => esc_html__( 'Enables show count total product of category.', 'strollik' ),
					'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
				),
				array(
					"type"       => "checkbox",
					"heading"    => esc_html__("show children of the current category",'strollik'),
					"param_name" => "show_children",
					'description' => esc_html__( 'Enables show children of the current category.', 'strollik' ),
					'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
				),
				array(
					"type"       => "checkbox",
					"heading"    => esc_html__("Show dropdown children of the current category ",'strollik'),
					"param_name" => "show_dropdown",
					'description' => esc_html__( 'Enables show dropdown children of the current category.', 'strollik' ),
					'value' => array( esc_html__( 'Yes, please', 'strollik' ) => 'yes' )
				),

				array(
					"type"        => "textfield",
					"heading"     => esc_html__("Extra class name",'strollik'),
					"param_name"  => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
				)
		   	)
		));
	 


		/**
		 * pbr_all_products
		 */
		 
		vc_map( array(
				'name' => esc_html__( 'PBR Product categories ', 'strollik' ),
				'base' => 'pbr_special_product_categories',
				'icon' => 'icon-wpb-woocommerce',
				'category' => esc_html__( 'PBR Woocommerce', 'strollik' ),
				'description' => esc_html__( 'Display product categories in carousel and sub categories', 'strollik' ),
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Per page', 'strollik' ),
						'value' => 12,
						'param_name' => 'per_page',
						'description' => esc_html__( 'How much items per page to show', 'strollik' ),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Columns', 'strollik' ),
						'value' => 4,
						'param_name' => 'columns',
						'description' => esc_html__( 'How much columns grid', 'strollik' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order by', 'strollik' ),
						'param_name' => 'orderby',
						'value' => $order_by_values,
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order way', 'strollik' ),
						'param_name' => 'order',
						'value' => $order_way_values,
						'description' => sprintf( esc_html__( 'Designates the ascending or descending order. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Category', 'strollik' ),
						'value' => $product_categories_dropdown,
						'param_name' => 'category',
						"admin_label" => true,
						'description' => esc_html__( 'Product category list', 'strollik' ),
					),
				)
			) );
	
		/**
		 * pbr_productcats_tabs
		 */
		$sortby = array(
            array('recent_product', esc_html__('Latest Products', 'strollik')),
            array('featured_product', esc_html__('Featured Products', 'strollik' )),
            array('best_selling', esc_html__('BestSeller Products', 'strollik' )),
            array('top_rate', esc_html__('TopRated Products', 'strollik' )),
            array('on_sale', esc_html__('Special Products', 'strollik' )),
            array('recent_review', esc_html__('Recent Products Reviewed', 'strollik' ))
        );
        $layout_type = array(
        	esc_html__('Carousel', 'strollik') => 'carousel',
        	esc_html__('Grid', 'strollik') => 'grid'
    	);
		vc_map( array(
				'name' => esc_html__( 'Categories Tabs ', 'strollik' ),
				'base' => 'pbr_categoriestabs',
				'icon' => 'icon-wpb-woocommerce',
				'category' => esc_html__( 'PBR Woocommerce', 'strollik' ),
				'description' => esc_html__( 'Display  categories in Tabs', 'strollik' ),
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Per page', 'strollik' ),
						'value' => 12,
						'param_name' => 'per_page',
						'description' => esc_html__( 'How much items per page to show', 'strollik' ),
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories (190px x 190px)", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Columns', 'strollik' ),
						'value' => 3,
						'param_name' => 'columns',
						'description' => esc_html__( 'How much columns grid', 'strollik' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order by', 'strollik' ),
						'param_name' => 'sortby',
						'std' => 'recent_product',
						'value' => $sortby,
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
					    'type' => 'autocomplete',
					    'heading' => esc_html__( 'Categories', 'strollik' ),
					    'value' => '',
					    'param_name' => 'categories',
					    "admin_label" => true,
					    'description' => esc_html__( 'Select Categories', 'strollik' ),
					    'settings' => array(
					     'multiple' => true,
					     'unique_values' => true,
					     // In UI show results except selected. NB! You should manually check values in backend
					    ),
				   	),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Layout Type', 'strollik' ),
						'param_name' => 'layout_type',
						'std' => 'carousel',
						'value' => $layout_type,
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),

				)
			) );

		/**
		 * pbr_productcats_tabs
		 */
		vc_map( array(
				'name' => esc_html__( 'Product Categories Tabs ', 'strollik' ),
				'base' => 'pbr_productcats_tabs',
				'icon' => 'icon-wpb-woocommerce',
				'category' => esc_html__( 'PBR Woocommerce', 'strollik' ),
				'description' => esc_html__( 'Display product categories in carousel and sub categories', 'strollik' ),
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Per page', 'strollik' ),
						'value' => 12,
						'param_name' => 'per_page',
						'description' => esc_html__( 'How much items per page to show', 'strollik' ),
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories (190px x 190px)", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Columns', 'strollik' ),
						'value' => 3,
						'param_name' => 'columns',
						'description' => esc_html__( 'How much columns grid', 'strollik' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order by', 'strollik' ),
						'param_name' => 'orderby',
						'std' => 'date',
						'value' => $order_by_values,
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order way', 'strollik' ),
						'param_name' => 'order',
						'std' => 'DESC',
						'value' => $order_way_values,
						'description' => sprintf( esc_html__( 'Designates the ascending or descending order. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Category', 'strollik' ),
						'value' => $product_categories_dropdown,
						"admin_label" => true,
						'param_name' => 'category',
						'description' => esc_html__( 'Product category list', 'strollik' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Layout Type', 'strollik' ),
						'param_name' => 'layout_type',
						'std' => 'carousel',
						'value' => $layout_type,
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
				)
			) );
		 
		/**
		 * pbr_productcats_normal
		 */

		vc_map( array(
				'name' => esc_html__( 'Product Categories Style 1 ', 'strollik' ),
				'base' => 'pbr_productcats_normal',
				'icon' => 'icon-wpb-woocommerce',
				'category' => esc_html__( 'PBR Woocommerce', 'strollik' ),
				'description' => esc_html__( 'Display product categories in carousel and sub categories', 'strollik' ),

				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Per page', 'strollik' ),
						'value' => 12,
						'param_name' => 'per_page',
						'description' => esc_html__( 'How much items per page to show', 'strollik' ),
						
					),
					array(
						"type"        => "attach_image",
						"description" => esc_html__("Upload an image for categories (190px x 190px)", 'strollik'),
						"param_name"  => "image_cat",
						"value"       => '',
						'heading'     => esc_html__('Image', 'strollik' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Image Float', 'strollik' ),
						'param_name' => 'image_float',
						'value' => array( esc_html__('Left','strollik') =>'pull-left', esc_html__('Right','strollik') =>'pull-right' ),
						'description' =>  esc_html__( 'Display banner image on left or right', 'strollik' ),
						'std' => 'pull-left'
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Columns', 'strollik' ),
						'value' => 3,
						'param_name' => 'columns',
						'description' => esc_html__( 'How much columns grid', 'strollik' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order by', 'strollik' ),
						'param_name' => 'orderby',
						'value' => $order_by_values,
						'std' => 'date',
						'description' => sprintf( esc_html__( 'Select how to sort retrieved products. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Order way', 'strollik' ),
						'param_name' => 'order',
						'value' => $order_way_values,
						'std' => 'DESC',
						'description' => sprintf( esc_html__( 'Designates the ascending or descending order. More at %s.', 'strollik' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Category', 'strollik' ),
						'value' => $product_categories_dropdown,
						'param_name' => 'category',
						'description' => esc_html__( 'Product category list', 'strollik' ),'admin_label'	=> true
					),
					array(
					"type"        => "textfield",
					"heading"     => esc_html__("Extra class name",'strollik'),
					"param_name"  => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
				)
				)
			) );
			
			vc_map( array(
			    "name" => esc_html__("PBR Products Collection",'strollik'),
			    "base" => "pbr_products_collection",
			    'description'=> esc_html__( 'Show products as bestseller, featured in block', 'strollik' ),
			    "class" => "",
			   	"category" => esc_html__('PBR Woocommerce','strollik'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title",'strollik'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => ''
					),
					 array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => esc_html__('Sub Title','strollik'),
		                "param_name" => "subtitle",
		            ),
					array(
					    'type' => 'autocomplete',
					    'heading' => esc_html__( 'Categories', 'strollik' ),
					    'value' => '',
					    'param_name' => 'categories',
					    "admin_label" => true,
					    'description' => esc_html__( 'Select Categories', 'strollik' ),
					    'settings' => array(
					     'multiple' => true,
					     'unique_values' => true,
					     // In UI show results except selected. NB! You should manually check values in backend
					    ),
				   	),
			    	array(
						"type" => "dropdown",
						"heading" => esc_html__("Type",'strollik'),
						"param_name" => "type",
						"value" => $product_type,
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Number Main Posts", 'strollik'),
						"param_name" => "num_mainpost",
						"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
						"std" => 1
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns count",'strollik'),
						"param_name" => "columns_count",
						"value" => $product_columns,
						"admin_label" => true,
						"description" => esc_html__("Select columns count.",'strollik')
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number of products to show",'strollik'),
						"param_name" => "number",
						"value" => '4'
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name",'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'strollik')
					)
			   	)
			));

		}
	}	

	/**
	  * Register Woocommerce Vendor which will register list of shortcodes
	  */
	function strollik_fnc_init_vc_woocommerce_vendor(){

		$vendor = new Strollik_VC_Woocommerce();
		add_action( 'vc_after_set_mode', array(
			$vendor,
			'load'
		) );

	}
	add_action( 'after_setup_theme', 'strollik_fnc_init_vc_woocommerce_vendor' , 9 );   
}		