<?php 

class Strollik_VC_Theme implements Vc_Vendor_Interface {

	public function load(){
		/*********************************************************************************************************************
		 *  Vertical menu
		 *********************************************************************************************************************/
		$option_menu  = array(); 
		if( is_admin() ){
			$menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
		    $option_menu = array('---Select Menu---'=>'');
		    foreach ($menus as $menu) {
		    	$option_menu[$menu->name]=$menu->term_id;
		    }
		}    
		vc_map( array(
		    "name" => esc_html__("PBR Quick Links Menu",'strollik'),
		    "base" => "pbr_quicklinksmenu",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    'description'	=> esc_html__( 'Show Quick Links To Access', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => 'Quick To Go'
				),
		    	array(
					"type" => "dropdown",
					"heading" => esc_html__("Menu", 'strollik'),
					"param_name" => "menu",
					"value" => $option_menu,
					"description" => esc_html__("Select menu.", 'strollik')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
		 
		

		/*********************************************************************************************************************
		 *  Vertical menu
		 *********************************************************************************************************************/
	 
		vc_map( array(
		    "name" => esc_html__("PBR Vertical MegaMenu",'strollik'),
		    "base" => "pbr_verticalmenu",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(

		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => 'Vertical Menu',
					"admin_label"	=> true
				),

		    	array(
					"type" => "dropdown",
					"heading" => esc_html__("Menu", 'strollik'),
					"param_name" => "menu",
					"value" => $option_menu,
					"description" => esc_html__("Select menu.", 'strollik')
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Position", 'strollik'),
					"param_name" => "postion",
					"value" => array(
							'left'=>'left',
							'right'=>'right'
						),
					'std' => 'left',
					"description" => esc_html__("Postion Menu Vertical.", 'strollik')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
		 
		vc_map( array(
		    "name" => esc_html__("Fixed Show Vertical Menu ",'strollik'),
		    "base" => "pbr_verticalmenu_show",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "description" => esc_html__( 'Always showing vertical menu on top', 'strollik' ),
		    "params" => array(
		  
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"description" => esc_html__("When enabled vertical megamenu widget on main navition and its menu content will be showed by this module. This module will work with header:Martket, Market-V2, Market-V3" , 'strollik')
				)
		   	)
		));
	 

		/******************************
		 * Our Team
		 ******************************/
		vc_map( array(
		    "name" => esc_html__("PBR Our Team Grid Style",'strollik'),
		    "base" => "pbr_team",
		    "class" => "",
		    "description" => 'Show Personal Profile Info',
		    "category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'strollik'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Job", 'strollik'),
					"param_name" => "job",
					"value" => 'CEO',
					'description'	=>  ''
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'strollik'),
					"param_name" => "information",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'strollik')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'strollik'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Google Plus", 'strollik'),
					"param_name" => "google",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'strollik'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'strollik'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Pinterest", 'strollik'),
					"param_name" => "pinterest",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Linked In", 'strollik'),
					"param_name" => "linkedin",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'strollik'),
					"param_name" => "style",
					'value' 	=> array( 'circle' => esc_html__('circle', 'strollik'), 'vertical' => esc_html__('vertical', 'strollik') , 'horizontal' => esc_html__('horizontal', 'strollik') ),
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)
		   	)
		));
	 
		/******************************
		 * Our Team
		 ******************************/
		vc_map( array(
			"name" => esc_html__("PBR Our Team List Style",'strollik'),
			"base" => "pbr_team_list",
			"class" => "",
			"description" => esc_html__('Show Info In List Style', 'strollik'),
			"category" => esc_html__('PBR Widgets', 'strollik'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'strollik'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'strollik'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'strollik'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Job", 'strollik'),
					"param_name" => "job",
					"value" => 'CEO',
					'description'	=>  ''
				),
				array(
					"type" => "textarea_html",
					"heading" => esc_html__("Information", 'strollik'),
					"param_name" => "content",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'strollik')
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Blockquote", 'strollik'),
					"param_name" => "blockquote",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Email", 'strollik'),
					"param_name" => "email",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'strollik'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'strollik'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Linked In", 'strollik'),
					"param_name" => "linkedin",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'strollik'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
				)

		   	)
		));
	 
		
	 

		/* Heading Text Block
		---------------------------------------------------------- */
		vc_map( array(
			'name'        => esc_html__( 'PBR Widget Heading','strollik'),
			'base'        => 'pbr_title_heading',
			"class"       => "",
			"category" => esc_html__('PBR Widgets', 'strollik'),
			'description' => esc_html__( 'Create title for one Widget', 'strollik' ),
			"params"      => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget title', 'strollik' ),
					'param_name' => 'title',
					'value'       => esc_html__( 'Title', 'strollik' ),
					'description' => esc_html__( 'Enter heading title.', 'strollik' ),
					"admin_label" => true
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'strollik' ),
				    'param_name' => 'font_color',
				    'description' => esc_html__( 'Select font color', 'strollik' )
				),
				 
				array(
					"type" => "textarea",
					'heading' => esc_html__( 'Description', 'strollik' ),
					"param_name" => "descript",
					"value" => '',
					'description' => esc_html__( 'Enter description for title.', 'strollik' )
			    ),
			    array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'strollik'),
					"param_name" => "style",
					'value' 	=> array( 'Default' => esc_html__('heading-default', 'strollik'),  'Style1' => esc_html__('heading-style1', 'strollik') ),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'strollik' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
				)
			),
		));
		
		/* Heading Text Block
		---------------------------------------------------------- */
		vc_map( array(
			'name'        => esc_html__( 'PBR Banner CountDown','strollik'),
			'base'        => 'pbr_banner_countdown',
			"class"       => "",
			"category" => esc_html__('PBR Widgets', 'strollik'),
			'description' => esc_html__( 'Show CountDown with banner', 'strollik' ),
			"params"      => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget title', 'strollik' ),
					'param_name' => 'title',
					'value'       => esc_html__( 'Title', 'strollik' ),
					'description' => esc_html__( 'Enter heading title.', 'strollik' ),
					"admin_label" => true
				),


				array(
					"type" => "attach_image",
					"description" => esc_html__("If you upload an image, icon will not show.", 'strollik'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> esc_html__('Image', 'strollik' )
				),


				array(
				    'type' => 'textfield',
				    'heading' => esc_html__( 'Date Expired', 'strollik' ),
				    'param_name' => 'input_datetime',
				    'description' => esc_html__( 'Select font color', 'strollik' ),
				),
				 

				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'strollik' ),
				    'param_name' => 'font_color',
				    'description' => esc_html__( 'Select font color', 'strollik' ),
				    'class'	=> 'hacongtien'
				),
				 
				array(
					"type" => "textarea",
					'heading' => esc_html__( 'Description', 'strollik' ),
					"param_name" => "descript",
					"value" => '',
					'description' => esc_html__( 'Enter description for title.', 'strollik' )
			    ),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'strollik' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'strollik' )
				),


				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Text Link', 'strollik' ),
					'param_name' => 'text_link',
					'value'		 => 'Find Out More',
					'description' => esc_html__( 'Enter your link text', 'strollik' )
				),


				
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Link', 'strollik' ),
					'param_name' => 'link',
					'value'		 => 'http://',
					'description' => esc_html__( 'Enter your link to redirect', 'strollik' )
				)
			),
		));

			//Testimonial
			vc_map( array(
			    "name" => esc_html__("PBR Testimonials",'strollik'),
			    "base" => "pbr_testimonials",
			    'description'=> esc_html__('Play Testimonials In Carousel', 'strollik'),
			    "class" => "",
			    "category" => esc_html__('PBR Widgets', 'strollik'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'strollik'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
							"admin_label" => true
					),

			    	array(
		               'type' => 'dropdown',
		               'heading' => esc_html__('Title font size', 'strollik' ),
		               'param_name' => 'size',
		               'value' => array(
		                    esc_html__('Large', 'strollik' ) 		=> 'font-size-lg',
		                    esc_html__('Medium', 'strollik' ) 		=> 'font-size-md',
		                    esc_html__('Small', 'strollik' ) 		=> 'font-size-sm',
		                    esc_html__('Extra small', 'strollik' ) => 'font-size-xs'
		                )
		            ),

		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__('Title Alignment', 'strollik' ),
		                'param_name' => 'alignment',
		                'value' => array(
		                    esc_html__('Align left', 'strollik' ) 		=> 'separator_align_left',
		                    esc_html__('Align center', 'strollik' ) 	=> 'separator_align_center',
		                    esc_html__('Align right', 'strollik' ) 	=> 'separator_align_right'
		                )
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Number", 'strollik'),
						"param_name" => "number",
						"value" => '4',
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Skin", 'strollik'),
						"param_name" => "skin",
						"value" => array(
							'Slide' 								=> 'slide',				
							'Slide - Light Style' 			=> 'slide light-style',
							'Slide - Left' 					=> 'left',
							'Slide - Left - Light style' 	=> 'left light-style',
							'Slide - Avatar - Right'		=> 'avatar-right',
							'Side - Avatar - v2'				=> 'grid',
							'Side - Avatar - v3'				=> 'avatar-v3'
						),
						"admin_label" => true,
						"description" => esc_html__("Select Skin layout.", 'strollik')
					),
					 array(
		                'type' => 'dropdown',
		                'heading' => esc_html__('Display Pagination', 'strollik' ),
		                'param_name' => 'pagination',
		                'value' => array(
		                	esc_html__('Enable', 'strollik' ) 	=> 'true',
		                  esc_html__('Disable', 'strollik' ) 		=> 'false',
		                )
		            ),
					  array(
		                'type' => 'dropdown',
		                'heading' => esc_html__('Display Arrow', 'strollik' ),
		                'param_name' => 'arrow',
		                'value' => array(
		                	esc_html__('Enable', 'strollik' ) 	=> 'true',
		                  esc_html__('Disable', 'strollik' ) 		=> 'false',
		                )
		            ),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'strollik'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'strollik')
					)
			   )
			));


	}
}