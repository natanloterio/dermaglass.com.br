<?php

function strollik_fnc_import_remote_demos() { 
	return array(
		'strollik' => array( 'name' => 'strollik',  'source'=> 'http://wpsampledemo.com/strollik/strollik.zip' ),
	);
}

add_filter( 'pbrthemer_import_remote_demos', 'strollik_fnc_import_remote_demos' );



function strollik_fnc_import_theme() {
	return 'strollik';
}
add_filter( 'pbrthemer_import_theme', 'strollik_fnc_import_theme' );

function strollik_fnc_import_demos() {
	$folderes = glob( get_template_directory() .'/inc/import/*', GLOB_ONLYDIR ); 

	$output = array(); 

	foreach( $folderes as $folder ){
		$output[basename( $folder )] = basename( $folder );
	}
 	
 	return $output;
}
add_filter( 'pbrthemer_import_demos', 'strollik_fnc_import_demos' );

function strollik_fnc_import_types() {
	return array(
			'all' => 'All',
			'content' => 'Content',
			'widgets' => 'Widgets',
			'page_options' => 'Theme + Page Options',
			'menus' => 'Menus',
			'rev_slider' => 'Revolution Slider',
			'vc_templates' => 'VC Templates'
		);
}
add_filter( 'pbrthemer_import_types', 'strollik_fnc_import_types' );
