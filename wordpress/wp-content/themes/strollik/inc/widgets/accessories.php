<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author      Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015  http://wpopal.com/. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://wpopal.com/
 * @support  http://www.wpopal.com/questions/
 */

class Strollik_Accessories extends WP_Widget {
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'strollik_accessories',
            // Widget name will appear in UI
            esc_html__('PBR Accessoies Products', 'strollik'),
            // Widget description
            array( 'description' => esc_html__( 'Display accessories products. ', 'strollik' ), )
        );
        $this->widgetName = 'accessories';
    }

    public function widget( $args, $instance ) {
        extract( $args );
        extract( $instance );

        echo ($before_widget);
            wc_get_template( 'single-product/accessories.php', $instance );

        echo ($after_widget);
    }

    public function form( $instance ) {
        $defaults = array('title' => 'Accessories Products', 'category' => '' , 'number' => '8', 'columns' => '4');
        $instance = wp_parse_args((array) $instance, $defaults); 
        $args = array(
                'type' => 'post',
                'child_of' => 0,
                'parent' => '',
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => false,
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'number' => '',
                'taxonomy' => 'product_cat',
                'pad_counts' => false,
            );
            $categories = get_categories( $args );
            $cats = array();
            foreach ($categories as $category) {
                $cats[$category->term_id] = $category->name;
            }
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'strollik'); ?></label>
            <input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'category' )); ?>"><?php esc_html_e('Category:', 'strollik'); ?></label>
            <br>
            <select name="<?php echo esc_attr($this->get_field_name( 'category' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'category' )); ?>">
                <?php foreach ($cats as $key => $value): ?>
                    <option value="<?php echo esc_attr( $key ); ?>" <?php selected( $instance['category'], $key ); ?>><?php echo esc_html( $value ); ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('Number', 'strollik'); ?></label>
            <input class="widefat" type="text" value="<?php echo esc_attr( $instance['number'] ); ?>" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('columns')); ?>"><?php esc_html_e('Columns', 'strollik'); ?></label>
            <input class="widefat" type="text" value="<?php echo esc_attr( $instance['columns'] ); ?>" id="<?php echo esc_attr($this->get_field_id('columns')); ?>" name="<?php echo esc_attr($this->get_field_name('columns')); ?>" />
        </p>

<?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['category'] = $new_instance['category'];
        $instance['number'] = $new_instance['number'];
        $instance['columns'] = $new_instance['columns'];

        return $instance;
    }
}

register_widget( 'Strollik_Accessories' );