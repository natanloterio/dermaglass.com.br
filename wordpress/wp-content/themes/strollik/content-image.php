<?php
/**
 * The template for displaying posts in the Image post format
 *
 * @package WPOPAL
 * @subpackage strollik
 * @since Strollik 1.0
 */
?>
<div class="post-single">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<div class="entry-meta">
					 <?php echo get_avatar( get_the_author_meta( 'email' ), 90 ) ?>
					<div class="entry-date">
		                <?php the_time( 'd M , Y' ); ?>
		            </div>
		            <div class="author"><?php esc_html_e('by', 'strollik'); the_author_posts_link(); ?></div>
		            <div class="entry-category">
		                <?php esc_html_e('in', 'strollik'); the_category(); ?>
		            </div>
					<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
					<div class="comments-link"><?php comments_popup_link( esc_html__( 'Leave a comment', 'strollik' ), esc_html__( '1 Comment', 'strollik' ), esc_html__( '% Comments', 'strollik' ) ); ?></div>
					<?php endif; ?>

					<?php edit_post_link( esc_html__( 'Edit', 'strollik' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .entry-meta -->
			
			</div>
			<div class="col-md-9 col-sm-8">
			
			 	<div class="post-preview">
					<?php strollik_fnc_post_thumbnail(); ?>
					<span class="post-format">
						<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'image' ) ); ?>"><i class="fa fa-picture-o"></i></a>
					</span>
				</div>
				<header class="entry-header">
					
					<?php
						

						if ( is_single() ) :
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
						endif;
					?>

				</header><!-- .entry-header -->
				<div class="entry-content">
					<?php
						/* translators: %s: Name of current post */
						if(is_single()){
							the_content( sprintf(
								esc_html__( 'Continue reading %s', 'strollik').'<span class="meta-nav">&rarr;</span>',
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );
						}else{
							the_excerpt();
						}

						wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'strollik' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
						) );
					?>
				</div><!-- .entry-content -->
				<?php if(is_single()): ?>
					<?php the_tags( '<div class="tag-links pull-left">', '', '</div>' ); ?>

					<?php if( strollik_fnc_theme_options('blog-show-share-post', true) ){
						get_template_part( 'page-templates/parts/sharebox' );
					} ?>
					<div class="clearfix"></div>
					<?php
					// Previous/next post navigation.
					 strollik_fnc_post_nav(); ?>
			 	<?php else: ?>
			 		<a class="btn btn-default" href="<?php the_permalink(); ?>" title="<?php esc_html_e( 'Read More', 'strollik' ); ?>"><?php esc_html_e( 'Read More', 'strollik' ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</article><!-- #post-## -->
</div>

