<?php
$cat[] = $category;  
$loop = strollik_fnc_woocommerce_query('', $number, $cat);

$_total = $loop->post_count;
$_count = 1;
$_id = strollik_fnc_makeid();
?>
<div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play" data-ride="owlcarousel">         
    <div class="widget-text-heading"><h3 class="widget-heading heading-style1"><span class="heading-text"><?php echo trim( $title ); ?></span></h3></div>
    <?php if ( $_total > $columns ) { ?>
        <div class="carousel-controls carousel-controls-v3 carousel-hidden">
            <a class="left carousel-control carousel-md" href="#" data-slide="prev">
                <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control carousel-md" href="#" data-slide="next">
                <span class="fa fa-angle-right"></span>
            </a>
        </div> 
    <?php } ?>
     <div class="owl-carousel" data-slide="<?php echo esc_attr($columns); ?>" data-pagination="false" data-navigation="true">
        <?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
            <div class="item">
                <div class="products-grid products">
                    <div class="product">
                        
                        <?php wc_get_template_part( 'content', 'product-inner' ); ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
 
</div>    
<?php wp_reset_postdata(); ?>