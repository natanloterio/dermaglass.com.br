<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com?>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('nice-style'); ?>>
        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
        <?php endif; ?>
        <div class="post-container">
            <div class="blog-post-detail blog-post-grid">
                <figure class="entry-thumb">
                    <?php strollik_fnc_post_thumbnail(); ?>
                    
                </figure>
                <div class="information-post">
                <h3 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <div class="entry-meta">
                        <span class="author"><?php esc_html_e('by', 'strollik'); the_author_posts_link(); ?></span>
                        <span class="meta-sep"> | </span>
                        <span class="entry-date">
                            <?php the_time( 'd M , Y' ); ?>
                        </span>
                        <span class="meta-sep"> | </span>
                        <span class="comment-count">
                            <?php comments_popup_link(esc_html__(' 0 comment', 'strollik'), esc_html__(' 1 comment', 'strollik'), esc_html__(' % comments', 'strollik')); ?>
                        </span>                       
                    </div>   
                    
                </div>
            </div>
        </div>
    </article>
