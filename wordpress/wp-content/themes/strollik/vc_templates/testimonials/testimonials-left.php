<?php $job = get_post_meta( get_the_ID(), 'strollik_testimonial_job', true); ?>
<div class="testimonials-left">
<div class="testimonials-body">
    <div class="testimonials-quote"><?php the_content() ?></div>
    <div class="testimonials-profile">
        <div class="testimonials-avatar radius-x">
              <?php the_post_thumbnail('widget', '', 'class="radius-x"');?>
        </div> 
        <h4 class="name"> <?php the_title(); ?></h4>
        <?php if(!empty( $job)): ?>
	        <div class="job"><?php echo trim($job); ?></div>
	    <?php endif; ?>
    </div>                    
</div>
</div>