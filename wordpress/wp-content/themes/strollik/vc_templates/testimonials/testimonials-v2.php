<?php
    $subject = get_post_meta( get_the_ID(), 'strollik_testimonial_subject', true);
    $rating = get_post_meta( get_the_ID(), 'strollik_testimonial_rating', true);
    $job = get_post_meta( get_the_ID(), 'strollik_testimonial_job', true);
?>
<div class="testimonials-wrap">		
	<div class="testimonials-body">
		<?php if ( !empty($subject) ): ?>
            <h4><?php echo trim( $subject ); ?></h4>
        <?php endif; ?>
	    <div class="testimonials-quote"><?php the_content() ?></div>
		<div class="rating rating_<?php echo esc_attr( $rating ); ?>"></div>
	    <div class="testimonials-avatar radius-x">
		    <?php the_post_thumbnail('widget', '', 'class="radius-x"');?>
		</div>
	    <div class="testimonials-profile"> 
	        <h4 class="name"> <?php the_title(); ?></h4>
	        <?php if(!empty($job) ): ?>
	        	<div class="job"><?php echo trim($job); ?></div>
	    	<?php endif; ?>
	    </div>
	</div>		
</div>
