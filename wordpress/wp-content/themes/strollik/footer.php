<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WPOPAL
 * @subpackage strollik
 * @since Strollik 1.0
 */
$footer_profile =  apply_filters( 'strollik_fnc_get_footer_profile', 'default' );
if($footer_profile){
	$footer 		= get_post($footer_profile);
	$class_footer 	= isset($footer->post_name)?$footer->post_name:'default';
}else{
	$class_footer = 'default';
} 
?>

		</section><!-- #main -->
		<?php do_action( 'strollik_template_main_after' ); ?>
		<?php do_action( 'strollik_template_footer_before' ); ?>
		<footer id="pbr-footer" class="pbr-footer <?php echo esc_attr($class_footer);?>" role="contentinfo">
			<div class="inner">				
				<?php if( $footer_profile && $footer_profile != 'default' ) : ?>
					<div class="pbr-footer-profile">
						<?php strollik_fnc_render_post_content( $footer_profile ); ?>
					</div>
				<?php else: ?>
					<?php get_sidebar( 'footer' ); ?>
				<?php endif; ?>


				<?php get_sidebar( 'mass-footer-body' );  ?>	

				<section class="pbr-copyright">
					<div class="container">
						<?php do_action( 'strollik_fnc_credits' ); ?>
						<?php 
							$copyright_text =  strollik_fnc_theme_options('copyright_text', '');
							if(!empty($copyright_text)){
								echo do_shortcode($copyright_text);
							}else{
								 
								$devby = '<a target="_blank" href="http://wpopal.com">WpOpal Team</a>';
								printf( esc_html__( 'Proudly powered by %s. Developed by %s', 'strollik' ), 'WordPress', $devby );  
							}
						?>
					</div>	
				</section>
			</div>
		</footer><!-- #colophon -->
		

		<?php do_action( 'strollik_template_footer_after' ); ?>
		<?php get_sidebar( 'offcanvas' );  ?>
	</div>
</div>
	<!-- #page -->

<?php wp_footer(); ?>
</body>
</html>